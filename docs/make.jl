using AsyncDecOPF
using Documenter

makedocs(;
    modules=[AsyncDecOPF],
    authors="Alyssia Dong <alyssia.dong@ens-rennes.fr> and contributors",
    repo="https://gitlab.com/alyssiadong/AsyncDecOPF.jl/blob/{commit}{path}#L{line}",
    sitename="AsyncDecOPF.jl",
    format=Documenter.HTML(;
        prettyurls=get(ENV, "CI", "false") == "true",
        canonical="https://alyssiadong.gitlab.io/AsyncDecOPF.jl",
        assets=String[],
    ),
    pages=[
        "Home" => "index.md",
    ],
)
