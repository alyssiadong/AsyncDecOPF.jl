using PowerModels
using DecentralizedPowerModels
# include("src/AsyncDecOPF.jl")
using AsyncDecOPF
using LightGraphs, SimpleWeightedGraphs
using Ipopt
using PlotlyJS, ORCA
using JLD

file = "test/data/case118.m"
# file = "test/data/case57.m"
ds = instantiate_model_dec_erseghe(file, 
				PowerModels.ACPPowerModel, DecentralizedPowerModels.build_dec_opf_erseghe;
				monitoring=true
				)
# # ds = instantiate_model_dec_b2b(file, 
# # 				PowerModels.ACPPowerModel, DecentralizedPowerModels.build_dec_opf_b2b;
# # 				monitoring=true
# # 				)
asyncProb = AsyncDecOPF.async_problem(ds)
commPar = AsyncDecOPF.commParams(100,50.0,0.1)
# compPar = AsyncDecOPF.compParams(0.1 .*ones(length(ds.pms)))
compPar = AsyncDecOPF.compParams(ds)

algoPar = AsyncDecOPF.AsyncParams(asyncProb, 
	0.3, 3000)
algoPar = AsyncDecOPF.BusSyncParams(asyncProb, 
	0.3, 3000)
# algoPar = AsyncDecOPF.FullSyncParams(asyncProb, 
# 	3000)

m = AsyncDecOPF.run_simulation(20000,20, ds, asyncProb, 
	commPar, compPar, algoPar)
sol, monitor = m
sol

# ##
# ## test IO
# ##

# ipopt_solver = optimizer_with_attributes(Ipopt.Optimizer,
# 	"tol"=>1e-8, "print_level"=>2, "constr_viol_tol"=>1e-6)

# result_ac_opf = PowerModels.run_ac_opf(file, ipopt_solver)
# result_ac_opf = result_ac_opf["solution"]

# using DataFrames, JLD

# file = AsyncDecOPF.JLDFile("case118_erseghe_test.jld",
# 	"results/case118_erseghe_test.jld",
# 	"case118.m")
# #file = AsyncDecOPF.JLDFile("case118_erseghe_sigma0_5.jld",
# #	"results/case118_erseghe_sigma0_5.jld",
# #	"case118.m")
# #file = AsyncDecOPF.JLDFile("case118_erseghe_sigma0_5_tirages.jld",
# #	"E:/case118_erseghe_sigma0_5_tirages.jld",
# #	"case118.m")
# # file = AsyncDecOPF.JLDFile("case57_erseghe_test.jld",
# # 	"results/case57_erseghe_test.jld",
# # 	"case57.m")

# #df = file.df_params
# #alpha = df.alpha[1]
# #beta = df.beta[1]
# #commPar = AsyncDecOPF.commParams(alpha,beta,0.0)

#  #test = AsyncDecOPF.readRes(file)
# #DecentralizedPowerModels.gif_monitor(test[1]["data"], result_ac_opf, yrange = (-0.35, 0.2), name = "erseghe_delta0_1.gif")

# # # res = AsyncDecOPF.convergenceIteration(test, df, 3, "gen_comp")
#  #AsyncDecOPF.plotComparison(file)

#  #meanDelay = AsyncDecOPF.meanDelay(commPar, asyncProb)
# # # AsyncDecOPF.plotConvergenceTime(test, df, 10^(0.2), norm=meanDelay)
#  #AsyncDecOPF.plotConvergenceTime(test, df, 5e-3, meanDelay)
# #df_res, norm = AsyncDecOPF.plotConvergenceTime(file, 5e-3)
# #plotConvergenceTimeAbstract(file, df_res, norm)
# #AsyncDecOPF.plotConvergenceTime(file, 5e-3)
# #ORCA.savefig(AsyncDecOPF.plotConvergenceTime(file, 5e-3), "images/abstract_fig3.pdf", format="pdf", width=334, height=94)






# ################### Abstract figure
# ### df_res, norm = AsyncDecOPF.plotConvergenceTime(file, 5e-3) -> résultat dans "results/case118_erseghe_sigma0_5_tirages_mean.jld"
# ### save("results/case118_erseghe_sigma0_5_tirages_mean.jld", "df_res", df_res, "norm", norm)

# #df_res = load("results/case118_erseghe_sigma0_5_tirages_mean.jld", "df_res")
# #norm = load("results/case118_erseghe_sigma1_0_tirages_mean.jld", "norm")
# #norm = 1.0
# #AsyncDecOPF.plotConvergenceTimeAbstract(df_res, norm)

# AsyncDecOPF.plotConvergenceTimeComparison()