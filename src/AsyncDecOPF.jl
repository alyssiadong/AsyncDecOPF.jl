module AsyncDecOPF

import InfrastructureModels
import PowerModels
import PowerModels:run_ac_opf
import DecentralizedPowerModels:build_dec_opf_erseghe, DecentralizedSystem, LocalPowerModel

import Memento
Memento.setlevel!(Memento.getlogger(InfrastructureModels), "error")
Memento.setlevel!(Memento.getlogger(PowerModels), "error")

# Create our module level logger (this will get precompiled)
const _LOGGER = Memento.getlogger(@__MODULE__)

# Register the module level logger at runtime so that folks can access the logger via `getlogger(PowerModels)`
# NOTE: If this line is not included then the precompiled `PowerModels._LOGGER` won't be registered at runtime.
__init__() = Memento.register(_LOGGER)

"Suppresses information and warning messages output by PowerModels, for fine grained control use the Memento package"
function silence()
    Memento.info(_LOGGER, "Suppressing information and warning messages for the rest of this session.  Use the Memento package for more fine-grained control of logging.")
    Memento.setlevel!(Memento.getlogger(InfrastructureModels), "error")
    Memento.setlevel!(Memento.getlogger(PowerModels), "error")
    Memento.setlevel!(Memento.getlogger(DecentralizedPowerModels), "error")
end

"alows the user to set the logging level without the need to add Memento"
function logger_config!(level)
    Memento.config!(Memento.getlogger("DecentralizedPowerModels"), level)
end

include("PowerNetwork.jl")
include("CommunicationDelays.jl")
include("sim/asynchrony.jl")
include("sim/messages.jl")
include("sim/erseghe.jl")
include("sim/b2b.jl")
include("sim/monitor.jl")
include("sim/simulation.jl")
include("IO.jl")
include("export.jl")

end
