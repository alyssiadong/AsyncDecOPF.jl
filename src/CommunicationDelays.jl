﻿### Functions that return communication delay values depending on the type of
### model chosen : proportionnal to distance (for now).

### Defining a structure to easily pass network link parameters
struct commParams
    # Delay = (α*Dist + β)*(1 + (σ/3)*randn())
    α::Float64
    β::Float64
    σ::Float64

    function commParams(α::Number, β::Number, σ::Number)
        @assert (σ ≤ 1) && (σ≥0) "Sigma is the relative value of the standard deviation : \n   sigma_AB = meanDelay_AD*(1+sigma/3 * randn() )\n   Hence sigma must be between 0 and 1."
        new(Float64(α), Float64(β), Float64(σ))
    end
end
_unpack(x::commParams) = (x.α, x.β, x.σ)     # method to easily unpack commParams

### Communication delay
function commDelay(distance::Float64, param::commParams)
    ### The value μ of the deterministic models depends on the
    ### distance between two agents : μ = β + α*distance (affine model).
        
    α, β, σ = _unpack(param)   # Delay coefficients
    return max(1e-8, (α*distance + β)*(1+ (σ/3)*randn()) )
end

function commDelay(id_exchange::Array{Int}, param::commParams, 
    asyncProblem::async_problem)
    ### The value μ of the deterministic models depends on the
    ### distance between two agents : μ = β + α*distance (affine model).
    ### Distance is computed from the 2 agents indexes in 'id_exchange'.
        
    # Distance
    dist = distance(id_exchange, asyncProblem)

    # Mean value of the (gaussian part of the) stochastic model
    return commDelay(dist, param)
end

function distance(id_exchange::Array{Int}, asyncProblem::async_problem)
    @assert length(id_exchange) == 2 "Can only compute distance between two agents."

    return asyncProblem.comm_graph_region.weights[id_exchange[1], id_exchange[2]]
end

### Group delay values
function sampleDelayValues(id_agents::Array{Int}, id_master::Int,
    param::commParams, asyncProblem::async_problem)
    ### Sample one delay value between each agent identified in 'id_agents'
    ### and the master identified by 'id_master'. Deterministic model.

        # Computes the deterministic delay for each agent
        Delays = [commDelay([id_ag, id_master], param, asyncProblem) for id_ag∈id_agents]
        return Delays
end

### Single delay value
function sampleDelayValues(id_agent1::Int, id_agent2::Int,
     param::commParams, asyncProblem::async_problem)
    ### Sample delay value between two agents. Deterministic model.
        Delay = commDelay([id_agent1, id_agent2], param, asyncProblem)
        return Delay
end

### Minimum delay
function minimumDelay(param::commParams, asyncProblem::async_problem)
    ### Computes the minimum delay in the deterministic model, associated
    ### to the smallest distance between two agents that are commercial partners.
        
    comm_distances = asyncProblem.comm_graph_region.weights.nzval
    dist_min = minimum(comm_distances)
    new_param = commParams(param.α, param.β, 0.0)

    return commDelay(dist_min, new_param)
end

### Mean delay
function meanDelay(param::commParams, asyncProblem::async_problem)
    ### Computes the mean delay in the deterministic model between all 
    ### commercial bonds.

    comm_distances = asyncProblem.comm_graph_region.weights.nzval
    dist_mean = 1/length(comm_distances)*sum(comm_distances)
    new_param = commParams(param.α, param.β, 0.0)

    return commDelay(dist_mean, new_param)
end




################################################################################
# Computation delays
################################################################################

### Defining a structure to easily pass computation delays parameters
struct compParams
    computationDelays::Array{Number,1}

    function compParams(computationDelays::AbstractVector{<:Number})
        new(computationDelays)
    end
    function compParams(ds::DecentralizedSystem)
        # Deterministic computation delay based on the OPF benchmark 
        # documented in the jupyter notebook
        out = ones(length(ds.pms))
        for (k, reg) in ds.pms
            nbus = length(PowerModels.ref(reg.pm, :bus))
            nbranch = length(PowerModels.ref(reg.pm, :branch))
            ngen = length(PowerModels.ref(reg.pm, :gen))

            nvars = 2*nbus+4*nbranch+2*ngen
            # comptime = max(0.000001, 0.25*nvars + 8 + randn()*19.5) # stochastic but the computation time stays the same for each simulation
            comptime = max(0.000001, 0.25*nvars + 8)
            out[k] = comptime
        end
        new(out)
    end
end

### Single computation delay value
function sampleCompDelay(i::Int, compDelays::compParams)
    compDelays.computationDelays[i]
end