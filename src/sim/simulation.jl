################################################################################
# Simulation functions
################################################################################
@resumable function checkConvergence(env::Environment, Tfinal::Number, 
    δt::Number, ds::DecentralizedSystem{ErsegheType}, 
    centralized_results::Dict{Int,<:Any}, local_iter_counters::Dict{Int,<:Any},
    Eprim::Number)
    for (k, local_iter_counter) in local_iter_counters
        ds.pms[k].monitor["iteration_counter"] = DecentralizedPowerModels._initialize_local_monitor(local_iter_counter, Tfinal÷δt)
    end
    while (Eprim < abs(ds.vanishing_measure)) || (now(env)<150)
        @yield timeout(env, δt)
        update_residuals!(ds)
        monitor_values_dec!(ds, centralized_results, local_iter_counters)

        (now(env)%100 == 0) && Memento.info(_LOGGER,"            Time: $(now(env))")
        if now(env) ≥ Tfinal
            break
        end
    end
end
@resumable function checkConvergence(env::Environment, Tfinal::Number, 
    δt::Number, ds::DecentralizedSystem{B2BType}, 
    centralized_results::Dict{Int,<:Any}, local_iter_counters::Dict{Int,<:Any},
    Eprim::Number)
    for (k, local_iter_counter) in local_iter_counters
        ds.pms[k].monitor["iteration_counter"] = DecentralizedPowerModels._initialize_local_monitor(local_iter_counter, Tfinal÷δt)
    end
    while (Eprim < abs(ds.primal_res)) || (now(env)<30)
        @yield timeout(env, δt)
        update_residuals!(ds)
        monitor_values_dec!(ds, centralized_results, local_iter_counters)

        (now(env)%100 == 0) && Memento.info(_LOGGER,"            Time: $(now(env))")
        if now(env) ≥ Tfinal
            break
        end
    end
end

function run_simulation(T_final::Number, δt::Number, 
ds::DecentralizedSystem,
asyncProb::async_problem,
comPar::commParams, compPar::compParams,
algoPar::AlgoParams, Eprim::Number=1e-3)

    # Number of regions
    N_reg = length(ds.pms)

    # Simulation environment
    sim = Simulation()

    # Create mailboxes for each region and each bus∈Ok
    Mailboxes = Dict(k=>MailboxManager(sim, k, asyncProb) 
                    for k in keys(asyncProb.duplicates_bus_list)) 

    # Array that saves trigger processes for every agent
    # Trigs = Array{Process, 1}(undef, N_reg)
    Trigs = Dict{Int, Process}()

    # Centralized results for monitoring 
    centralized_results = Dict(k=>Dict{String,Any}() 
                    for k in keys(asyncProb.duplicates_bus_list))

    # Local iteration counters
    if ds isa DecentralizedSystem{ErsegheType}
        local_iter_counters = Dict{Int,Any}(k =>
                                                Dict{Int,Any}( j => 
                                                    Dict{Int,Any}( h => 1 for h in dup_reg) 
                                                for (j, dup_reg) in asyncProb.duplicates_bus_list[k]["j"])
                                            for k in keys(asyncProb.duplicates_bus_list))
    elseif ds isa DecentralizedSystem{B2BType}
        local_iter_counters = Dict{Int,Any}(k =>
                                                Dict{Int,Any}( j => 
                                                    Dict( idx => 1 for (idx,info) in dic["buspair"]) 
                                                    for (j, dic) in asyncProb.duplicates_bus_list[k]["j"])
                                            for k in keys(asyncProb.duplicates_bus_list))
    else
        error("Not implemented yet.")
    end 

    # Check convergence
    conv = @process checkConvergence(sim, T_final, δt, ds, centralized_results, local_iter_counters, Eprim)

    # Launch process for every agent
    for i∈1:N_reg
        Trigs[i] = @process region_agent(
            sim, i, ds.pms[i], Mailboxes, Trigs, centralized_results, local_iter_counters[i],
            asyncProb, comPar, compPar, algoPar)
    end

    res, comp_time, bytes, gctime, memallocs = @timed begin
        try
            run(sim, conv)
        catch ex 
            println("Error at ", now(sim))
            display(active_process(sim))
            println("")
            rethrow(ex)
        end
    end
    Memento.info(_LOGGER, "OPF stoped at $(now(sim))")

    DecentralizedPowerModels.truncate_monitor_values!(ds)
    results = update_local_solution!(ds, nothing, [])
    sol = build_global_solution(results,ds)
    monitor = build_global_monitor(ds)

    monitor_out = Dict{String,Any}()
    monitor_out["sampling_time"] = δt
    monitor_out["max_time"] = T_final
    monitor_out["final_time"] = now(sim)
    monitor_out["final_sample"] = ds.pms[1].monitor_time
    monitor_out["max_sample"] = ds.pms[1].monitor_max_time
    monitor_out["final_Eprim"] = Eprim

    sim_dic = Dict{String,Any}()
    sim_dic["elapsed_time"] = comp_time
    sim_dic["bytes"] = bytes
    sim_dic["gctime"] = gctime
    monitor_out["simulation"] = sim_dic
    monitor_out["data"] = monitor

    return sol, monitor_out
end