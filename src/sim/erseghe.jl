@resumable function region_agent(env::Environment, k::Int,
lpm::ErsegheLPM,
Mailboxes::Dict{Int,<:MailboxManager}, Trigs::Dict{Int,Process},
centralized_results::Dict{Int,<:Any}, local_iter_counter::Dict{Int,<:Any},
asynProb::async_problem, nPar::commParams, cPar::compParams, algoPar::AlgoParams)
    #######################################################
    ### Initialization
    #######################################################

    pm = lpm.pm
    duplicates_bus_list = asynProb.duplicates_bus_list[k]["j"]
    duplicates_bus_list_keys = collect(keys(asynProb.duplicates_bus_list[k]["j"]))
    bus_list = ids(pm, :bus)
    neighbor_regions_list = neighbors(asynProb.comm_graph_region, k)
    optimizer = optimizer_with_attributes(Ipopt.Optimizer, 
            "tol"=>1e-8, "print_level"=>0, "constr_viol_tol"=>1e-6)
    local_mailbox = Mailboxes[k]

    local_iteration = 1

    #######################################################
    ### First iteration
    #######################################################
    Memento.info(_LOGGER, "Region $k first iteration...")

    # First local problem resolution to set optimizer
    results = update_local_solution!(pm, optimizer,[])
    centralized_results[k] = results

    # Initialize local voltage copies
    buses = [j for (j,dup_reg) in duplicates_bus_list
                    if k∈dup_reg]
    for j in buses
        update_voltage_copy_variables!(lpm, j, k, _read_complex_voltage_init(lpm, j))
    end

    # Send messages to neighbor regions with initialized values
    for h in neighbor_regions_list
        buses = [ j for (j,dup_reg) in duplicates_bus_list
                    if h∈dup_reg ]
        datas = [ _read_complex_voltage_init(lpm, j)   for j in buses ]
        iters = [ 1                     for j in buses ]

        delay = sampleDelayValues(k,h,nPar,asynProb)
        @process sendMessageInit(env, 
            k,h,buses,iters,datas,delay,
            Mailboxes, Trigs, algoPar, asynProb
            )
    end

    # Wait for iteration trigger
    trig = @process waitForTrigger(env, k, algoPar)
    try
        @yield trig
    catch 
        # println("Region $k init timeout interrupted")
    end

    # Read messages 
    messages_back_temp = Dict{Int,Any}() 
    updateReadyFlags(local_mailbox,k,algoPar)
    for (j,reg_list) in duplicates_bus_list
        messages_back_reg_temp = Dict{Int, Any}()
        for reg in reg_list
            proc = @process readMessage(env, j, reg, local_mailbox)
            mes = @yield proc
            message, messages_back = mes
            update_voltage_copy_variables!(lpm, message)
            messages_back_reg_temp[reg] = messages_back
        end
        messages_back_temp[j] = messages_back_reg_temp
    end
    bus_reg_iter_update = deepcopy(duplicates_bus_list)

    # Initilialize local dual variables
    initialize_dual_variables!(lpm)

    # Update objective function
    update_objective_functions!(lpm)

    # Init done
    local_mailbox.init_done_flag = true
    Memento.info(_LOGGER,"       Init $k done.")

    ######################################################
    ## Region's process
    ######################################################
    while true
        # Increment iterations (all duplicated regions per bus)
        for (j,reg_j) in bus_reg_iter_update
            for h in reg_j
                local_iter_counter[j][h] += 1
            end
        end
        updateLocalIterCounter(local_iter_counter, local_mailbox)
        local_iteration += 1

        # println("region $k iter $local_iteration")

        (local_iteration %50 == 0) && Memento.info(_LOGGER,"Region $k iter $local_iteration")

        # Put messages back in mailbox
        for (j, bus_dic) in messages_back_temp
            for (h, messages) in bus_dic
                for mes in messages
                    @yield @process leaveMessage(env, mes, local_mailbox)
                end
            end
        end

        # println("test1")

        # Update local solution
        results = update_local_solution!(pm, nothing, [])
        centralized_results[k] = results

        # Update local voltage copies
        for j in bus_list
            if !(j in duplicates_bus_list_keys) || (j in keys(bus_reg_iter_update))
                update_voltage_copy_variables!(lpm, j, k, 
                    _read_complex_voltage(lpm, j))
            end
        end

        # Computation delay
        #   if the timeout is interrupted by the arrival of a new message,
        #   it must be launched again.
        comp_delay = sampleCompDelay(k, cPar)
        if comp_delay > 0 
            comp_tend = now(env) + comp_delay
            while comp_tend > now(env)
                try
                    @yield timeout(env, comp_tend - now(env))
                catch
                    nothing
                    # Memento.info(_LOGGER, "Region $k received a message during its computation time")
                end
            end
        end

        # Send messages back
        for h in neighbor_regions_list
            buses = [ j for (j,reg_j) in bus_reg_iter_update
                        if h∈reg_j ]
            datas = [ _read_complex_voltage(lpm, j)     for j in buses ]
            iters = [ local_iter_counter[j][h]                      for j in buses ]

            delay = sampleDelayValues(k,h,nPar,asynProb)
            @process sendMessage(env, 
                k,h,buses,iters,datas,delay,
                Mailboxes, Trigs, algoPar,
                )
        end

        # # Monitor values
        # PowerModels.monitor_values!(lpm, results)

        # # If the number of received messages exceeds ntrig, no need to wait for the trigger.   
        if checkTrigCondition(local_mailbox, algoPar, k)
            nothing
        else
            trig = @process waitForTrigger(env, k, algoPar)
            try
                @yield trig
            catch 
                # println("Region $k timeout interrupted")
            end
        end

        # Read messages 
        messages_back_temp      = Dict{Int,Any}() 
        bus_reg_iter_update     = Dict{Int, Any}()
        updateReadyFlags(local_mailbox,k,algoPar)
        for (j, reg_list) in duplicates_bus_list
            try
                @yield @process retrieveBusMessages(env, k, j, reg_list, 
                    messages_back_temp, bus_reg_iter_update,
                    local_mailbox, algoPar, lpm)
            catch ex
                Memento.warn(_LOGGER, "Region $k process interrupted during its iteration.")
                # rethrow(ex)
            end
            # display(bus_reg_iter_update)
        end
        # println("test3_0")
        # display(bus_reg_iter_update)
        # println("test3")
        # Update dual variables
        for (j, reg_j) in bus_reg_iter_update
            # println("test4")
            update_dual_variables!(lpm, j, reg_j)
        end

        # Update objective function
        for (j, reg_j) in bus_reg_iter_update
            update_objective_functions!(lpm, j)
        end
    end
end



##
### Update bus j voltage copy variable with latest information from region h
##
update_voltage_copy_variables!(lpm::ErsegheLPM, mes::Message) = 
    update_voltage_copy_variables!(lpm, bus(mes), senderRegion(mes), data(mes))
update_voltage_copy_variables!(lpm::ErsegheLPM, j::Int, h::Int, val) = 
    update_voltage_copy_variables!(lpm, lpm.pm, j, h, val)
function update_voltage_copy_variables!(lpm::ErsegheLPM, pm::AbstractPowerModel, j::Int, h::Int, val)
    lpm.copyVariables["voltage_prev"][j][h] = lpm.copyVariables["voltage"][j][h]
    lpm.copyVariables["voltage"][j][h] = val
end
