@resumable function region_agent(env::Environment, k::Int,
lpm::B2BLPM,
Mailboxes::Dict{Int,<:MailboxManager}, Trigs::Dict{Int,Process},
centralized_results::Dict{Int,<:Any}, local_iter_counter::Dict{Int,<:Any},
asynProb::async_problem, nPar::commParams, cPar::compParams, algoPar::AlgoParams)
    Memento.error(_LOGGER, "Must implement computation delay !")

    #######################################################
    ### Initialization
    #######################################################

    pm = lpm.pm
    duplicates_bus_list = asynProb.duplicates_bus_list[k]["j"]
    duplicates_bus_list_keys = collect(keys(asynProb.duplicates_bus_list[k]["j"]))
    bus_list = ids(pm, :bus)
    neighbor_regions_list = neighbors(asynProb.comm_graph_region, k)
    optimizer = optimizer_with_attributes(Ipopt.Optimizer, 
            "tol"=>1e-8, "print_level"=>2, "constr_viol_tol"=>1e-6)
    local_mailbox = Mailboxes[k]

    # local_iter_counter = Dict{Int,Any}( j => 
    #         Dict( idx => 1 for (idx,info) in dic["buspair"]) 
    #         for (j, dic) in duplicates_bus_list)

    local_iteration = 1

    #######################################################
    ### First iteration
    #######################################################
    Memento.info(_LOGGER, "Region $k first iteration...")

    # First local problem resolution to set optimizer
    results = update_local_solution!(pm, optimizer,[])
    centralized_results[k] = results

    # Initialize local voltage copies
    for (j,dic) in duplicates_bus_list
        for (idx,info) in dic["buspair"]
            update_voltage_copy_variables!(lpm, idx, _read_complex_voltage_init(lpm, idx))
        end
    end

    # Send messages to neighbor regions with initialized values
    for h in neighbor_regions_list
        indexes =   vcat([ [idx for (idx,info) in dic["buspair"] if info["connected_region"] == h] for (j,dic) in duplicates_bus_list ]...)
        datas =     [ _read_complex_voltage_init(lpm, idx)  for idx in indexes ]
        iters =     [ 1                                     for idx in indexes ]

        delay = sampleDelayValues(k,h,nPar,asynProb)
        @process sendMessageInit(env, 
            k,h,indexes,iters,datas,delay,
            Mailboxes, Trigs, algoPar, asynProb
            )
    end

    # Wait for iteration trigger
    try
        trig = @process waitForTrigger(env, k, algoPar)
        @yield trig
    catch 
        # println("Region $k init timeout interrupted at $(now(env))")
    end

    # Read messages 
    messages_back_temp = Dict{Int,Any}() 
    updateReadyFlags(local_mailbox,k,algoPar)
    for (j,dic) in duplicates_bus_list
        messages_back_idx_temp = Dict()
        for (idx, info) in dic["buspair"]
            i,j2,dir = idx
            proc = @process readMessage(env, j, (j2,i,3-dir), local_mailbox)
            mes = @yield proc
            message, messages_back = mes
            update_voltage_copy_variables!(lpm, message)
            if length(messages_back)>0
                messages_back_idx_temp[idx] = messages_back
            end
        end
        if length(messages_back_idx_temp)>0
            messages_back_temp[j] = messages_back_idx_temp
        end
    end
    bus_idx_iter_update = deepcopy(duplicates_bus_list)


    # Initilialize local dual variables
    initialize_dual_variables!(lpm)

    # Update objective function
    update_objective_functions!(lpm)

    # Init done
    local_mailbox.init_done_flag = true
    Memento.info(_LOGGER,"       Init $k done.")

    #######################################################
    ### Region's process
    #######################################################
    while true
        # Increment iterations (all duplicated regions per bus)
        for (j,dic) in bus_idx_iter_update
            for (idx,info) in dic["buspair"]
                local_iter_counter[j][idx] += 1
            end
        end
        updateLocalIterCounter(local_iter_counter, local_mailbox)

        local_iteration += 1
        (local_iteration %50 == 0) && Memento.info(_LOGGER, "Region $k iter $local_iteration")

        # Put messages back in mailbox
        for (j, bus_dic) in messages_back_temp
            for (idx, messages) in bus_dic
                for mes in messages
                    @yield @process leaveMessage(env, mes, local_mailbox)
                end
            end
        end

        # Update local solution
        results = update_local_solution!(pm, nothing, [])
        centralized_results[k] = results

        # Update local voltage copies
        for (j,dic) in bus_idx_iter_update
            vj = _read_complex_voltage(lpm, j)
            for (idx,info) in dic["buspair"]
                update_voltage_copy_variables!(lpm, idx, vj)
            end
        end

        # Send messages back
        for h in neighbor_regions_list
            indexes =   vcat([ [idx for (idx,info) in dic["buspair"] if info["connected_region"] == h] for (j,dic) in bus_idx_iter_update ]...)
            datas = [ _read_complex_voltage(lpm, idx)                       for idx in indexes ]
            iters = [ local_iter_counter[(idx[1],idx[2])[idx[3]]][idx]      for idx in indexes ]

            delay = sampleDelayValues(k,h,nPar,asynProb)
            @process sendMessage(env, 
                k,h,indexes,iters,datas,delay,
                Mailboxes, Trigs, algoPar,
                )
        end

        # # Monitor values
        # PowerModels.monitor_values!(lpm, results)

        # Trigger verification ?

        # Wait for iteration trigger
        trig = @process waitForTrigger(env, k, algoPar)
        try
            @yield trig
        catch 
            # println("Region $k timeout interrupted")
        end

        # Read messages 
        messages_back_temp      = Dict{Int,Any}() 
        bus_idx_iter_update     = Dict{Int, Any}()
        updateReadyFlags(local_mailbox,k,algoPar)
        for (j, dic) in duplicates_bus_list
            @yield @process retrieveBusMessages(env, k, j, dic["buspair"], 
                messages_back_temp, bus_idx_iter_update,
                local_mailbox, algoPar, lpm)
        end

        # Update dual variables
        for (j, dic) in bus_idx_iter_update
            for (idx,info) in dic["buspair"]
                i,j1,dir = idx
                update_dual_variables!(lpm, i, j1, dir)
            end
        end

        # Update objective function
        for (j, dic) in bus_idx_iter_update
            for (idx,info) in dic["buspair"]
                i,j1,dir = idx
                update_objective_functions!(lpm, i, j1, dir)
            end
        end
    end
end


update_voltage_copy_variables!(lpm::B2BLPM, mes::Message) = 
    update_voltage_copy_variables!(lpm, idx(mes), data(mes))
update_voltage_copy_variables!(lpm::B2BLPM, idx, val) = 
    update_voltage_copy_variables!(lpm, lpm.pm, idx, val)
function update_voltage_copy_variables!(lpm::B2BLPM, pm::AbstractPowerModel, idx, val)
    lpm.copyVariables["voltage_prev"][idx] = lpm.copyVariables["voltage"][idx]
    lpm.copyVariables["voltage"][idx] = val
end
