function monitor_values_dec!(ds::DecentralizedSystem, res::Dict, iter_counters::Dict)
	for (k, iter_counter) in iter_counters
		res[k]["solution"]["iteration_counter"] = copy_dic_string(iter_counter)
	end
    monitor_values!(ds, res)
end

# Deep copy of a dictionary but replacing keys by strings
function copy_dic_string(var::Dict)
	out = Dict{String,Any}()
	for (key,val) in var
		out[string(key)] = copy_dic_string(val)
	end
	return out
end
function copy_dic_string(var)
	return var
end