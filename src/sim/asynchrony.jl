using OSQP
using LinearAlgebra, SparseArrays
using ResumableFunctions, SimJulia
using HDF5
using LightGraphs, SimpleWeightedGraphs

import PowerModels, DecentralizedPowerModels
import PowerModels: ids
import PowerModels: ACRPowerModel, ACPPowerModel, AbstractPowerModel, optimizer_with_attributes
import DecentralizedPowerModels: update_voltage_copy_variables!, update_local_solution!, update_objective_functions!
import DecentralizedPowerModels: initialize_dual_variables!, update_dual_variables!
import DecentralizedPowerModels: update_residuals!, monitor_values!, build_global_solution, build_global_monitor
import DecentralizedPowerModels: _read_complex_voltage_init, _read_complex_voltage
import DecentralizedPowerModels: ProblemType

import Base.show
export show
export AlgoParams

################################################################################
# ADMM algorithm parameters, exceptions, information structures
################################################################################

abstract type AlgoParams{T<:ProblemType} end

struct FullSyncParams{T} <: AlgoParams{T}
    N_trig_tot::Dict{Int, Int}
    T_max::Dict{Int, Number}

    # Constructor
    function FullSyncParams( asyncProb::async_problem{<:ErsegheType}, T_max)
        N_trig_tot = Dict( k => sum( length(reg_j) for (j,reg_j) in dic_k["j"]) 
            for (k,dic_k) in asyncProb.duplicates_bus_list)

        T_max_dic = Dict(k=> T_max
                for k in vertices(asyncProb.comm_graph_region))

        new{ErsegheType}(N_trig_tot, T_max_dic)
    end
    function FullSyncParams( asyncProb::async_problem{<:B2BType}, T_max)
        N_trig_tot = Dict( k => sum( length(reg_j["buspair"]) for (j,reg_j) in dic_k["j"]) 
            for (k,dic_k) in asyncProb.duplicates_bus_list)

        T_max_dic = Dict(k=> T_max
                for k in vertices(asyncProb.comm_graph_region))

        new{B2BType}(N_trig_tot, T_max_dic)
    end
end

struct BusSyncParams{T} <: AlgoParams{T}
    N_trig_bus::Dict{Int,Dict{Int,Int64}}             # Trigger parameters
    N_trig_reg::Dict{Int,Int64}     
    T_max::Dict{Int,Number}           

    # Constructor
    function BusSyncParams( asyncProb::async_problem{<:ErsegheType}, δ_reg, T_max)
        N_trig_reg = Dict(k=> ceil(δ_reg*length(dic_k["j"]))
                for (k,dic_k) in asyncProb.duplicates_bus_list)

        N_trig_bus = Dict(k=>Dict(j=>length(reg_j)
                for (j,reg_j) in dic_k["j"])
                for (k,dic_k) in asyncProb.duplicates_bus_list)

        T_max_dic = Dict(k=> T_max
                for k in vertices(asyncProb.comm_graph_region))

        new{ErsegheType}(N_trig_bus, N_trig_reg, T_max_dic)
    end
    function BusSyncParams( asyncProb::async_problem{<:B2BType}, δ_reg, T_max)
        N_trig_reg = Dict(k=> ceil(δ_reg*length(dic_k["j"]))
                for (k,dic_k) in asyncProb.duplicates_bus_list)

        N_trig_bus = Dict(k=>Dict(j=>length(reg_j["buspair"])
                for (j,reg_j) in dic_k["j"])
                for (k,dic_k) in asyncProb.duplicates_bus_list)

        T_max_dic = Dict(k=> T_max
                for k in vertices(asyncProb.comm_graph_region))

        new{B2BType}(N_trig_bus, N_trig_reg, T_max_dic)
    end
end

struct AsyncParams{T} <: AlgoParams{T}
    N_trig::Dict{Int, Int}
    T_max::Dict{Int, Number}

    # Constructor
    function AsyncParams( asyncProb::async_problem{<:ErsegheType}, δ, T_max)
        N_trig = Dict( k => ceil(δ*sum( length(reg_j) for (j,reg_j) in dic_k["j"])) 
                for (k,dic_k) in asyncProb.duplicates_bus_list)

        T_max_dic = Dict(k=> T_max
                for k in vertices(asyncProb.comm_graph_region))

        new{ErsegheType}(N_trig, T_max_dic)
    end
    function AsyncParams( asyncProb::async_problem{<:B2BType}, δ, T_max)
        N_trig = Dict( k => ceil(δ*sum( length(reg_j["buspair"]) for (j,reg_j) in dic_k["j"])) 
                for (k,dic_k) in asyncProb.duplicates_bus_list)

        T_max_dic = Dict(k=> T_max
                for k in vertices(asyncProb.comm_graph_region))

        new{B2BType}(N_trig, T_max_dic)
    end
end

struct UnconsistentPartner <: Exception end