# ################################################################################
# # Message structure and trigger
# ################################################################################
    
struct Message
    k::Int                  # Region sending the message
    h::Int                  # Region receiving the message
    bus::Int                # Bus
    iter::Int               # Link iteration
    idx
    data                    # List of data: vkj, j∈buses

    function Message(k::Int,h::Int,bus::Int,iter::Int,data)
        idx = (0,0,0)
        new(k,h,bus,iter,idx,data)
    end
    function Message(k::Int,h::Int,bus::Int,iter::Int,idx::Tuple,data)
        new(k,h,bus,iter,idx,data)
    end
end
receiverRegion(mes::Message) = mes.h
senderRegion(mes::Message) = mes.k
bus(mes::Message) = mes.bus
iter(mes::Message) = mes.iter
idx(mes::Message) = mes.idx
data(mes::Message) = mes.data

function show(io::IO, mes::Message)
    println("Message from ", mes.k, " to ", mes.h, " about bus ", mes.bus)
end

@resumable function waitForTrigger(env::Environment, k::Int, algoPar::AlgoParams)
    ### Next iteration trigger for region k : 
    ###     has received N = N_trig messages (by interruption)
    ###     OR t ≥ Tmax

    # t0 = now(env)
    T_max = algoPar.T_max[k]
    @yield timeout(env, T_max)
    # Memento.warn(_LOGGER, "Timeout set at $t0 triggered for region $k at $(now(env))")
end

mutable struct MailboxManager{T<:ProblemType}       # One MailboxManager for each region
    counter::Dict{Int, Any}
    matching_counter::Dict{Int, Any}
    mailbox::Dict{Int, Any}
    local_iter_counter::Dict{Int, Any}
    ready_flag::Dict{Int, Bool}
    init_done_flag::Bool

    function MailboxManager(env::Environment, k_region::Int, asyncProb::async_problem{<:ErsegheType})
        dup_list = asyncProb.duplicates_bus_list[k_region]["j"]
        counter = Dict(j=> Dict( h=>0 for h in val) for (j,val) in dup_list)
        matching_counter = Dict(j=> Dict( h=>0 for h in val) for (j,val) in dup_list)
        mailbox = Dict(j=> Dict( h=>Store{Message}(env) for h in val) for (j,val) in dup_list)
        local_iter_counter = Dict(j=> Dict( h=>1 for h in val) for (j,val) in dup_list)
        ready_flag = Dict(j=> false for (j,val) in dup_list)
        init_done_flag = false

        new{ErsegheType}(counter, matching_counter, mailbox, local_iter_counter, ready_flag, init_done_flag)
    end
    function MailboxManager(env::Environment, k_region::Int, asyncProb::async_problem{<:B2BType})
        dup_list = asyncProb.duplicates_bus_list[k_region]["j"]
        counter = Dict(j=> Dict( (j,i,3-dir)=>0 for ((i,j,dir),info) in val["buspair"]) for (j,val) in dup_list)
        matching_counter = Dict(j=> Dict( (j,i,3-dir)=>0 for ((i,j,dir),info) in val["buspair"]) for (j,val) in dup_list)
        mailbox = Dict(j=> Dict( (j,i,3-dir)=>Store{Message}(env) for ((i,j,dir),info) in val["buspair"]) for (j,val) in dup_list)
        local_iter_counter = Dict(j=> Dict( (i,j,dir)=>1 for ((i,j,dir),info) in val["buspair"]) for (j,val) in dup_list)
        ready_flag = Dict(j=> false for (j,val) in dup_list)
        init_done_flag = false

        new{B2BType}(counter, matching_counter, mailbox, local_iter_counter, ready_flag, init_done_flag)
    end
end
countMatchingMessages(mailbox::MailboxManager) =    sum( sum(c for (h,c) in dic_j) for (j,dic_j) in mailbox.matching_counter)
countMessages(mailbox::MailboxManager) =            sum( sum(c for (h,c) in dic_j) for (j,dic_j) in mailbox.counter)
countMatchingMessagesBuswise(mailbox::MailboxManager) = Dict( j => sum(c for (h,c) in dic_j) for (j,dic_j) in mailbox.matching_counter)
countMatchingMessagesBuswise(mailbox::MailboxManager, j::Int) = sum(c for (h,c) in mailbox.matching_counter[j])

function _incrementMessageCount(mailbox::MailboxManager, j::Int, idx, matching::Bool)
    mailbox.counter[j][idx] += 1
    mailbox.matching_counter[j][idx] += (matching ? 1 : 0)
end

function _decrementMessageCount(mailbox::MailboxManager, j::Int, idx, matching::Bool)
    mailbox.counter[j][idx] -= 1
    mailbox.matching_counter[j][idx] -= (matching ? 1 : 0)
end

@resumable function leaveMessage(env::Environment, k::Int, h::Int, bus::Int, iter::Int, data, 
mailbox::MailboxManager{ErsegheType})
    matching = (mailbox.local_iter_counter[bus][k] == iter)
    _incrementMessageCount(mailbox, bus, k, matching)
    @yield put(mailbox.mailbox[bus][k], Message(k,h,bus,iter,data))
end
@resumable function leaveMessage(env::Environment, mes::Message, mailbox::MailboxManager{ErsegheType})
    matching = (mailbox.local_iter_counter[bus(mes)][senderRegion(mes)] == iter(mes))
    _incrementMessageCount(mailbox, bus(mes), senderRegion(mes), matching)
    @yield put(mailbox.mailbox[bus(mes)][senderRegion(mes)], mes)
end
@resumable function leaveMessage(env::Environment, idx, k::Int, h::Int, iter::Int, data, 
mailbox::MailboxManager{B2BType})
    i,j,dir = idx
    bus = (i,j)[dir]
    matching = (mailbox.local_iter_counter[bus][(j,i,3-dir)] == iter)
    _incrementMessageCount(mailbox, bus, idx, matching)
    @yield put(mailbox.mailbox[bus][idx], Message(k,h,bus,iter,idx,data))
end
@resumable function leaveMessage(env::Environment, mes::Message, mailbox::MailboxManager{B2BType})
    i,j,dir = idx(mes)
    matching = (mailbox.local_iter_counter[bus(mes)][(j,i,3-dir)] == iter(mes))
    # println("$(mailbox.local_iter_counter[bus(mes)][idx(mes)]) == $(iter(mes)) from region $(senderRegion(mes)) to $(receiverRegion(mes))")
    _incrementMessageCount(mailbox, bus(mes), idx(mes), matching)
    @yield put(mailbox.mailbox[bus(mes)][idx(mes)], mes)
end

@resumable function readMessage(env::Environment, bus::Int, reg::Int,
mailbox::MailboxManager{ErsegheType})
    # messages: contains messages taken into account in this iteration
    # messages_back: contains messages taken into account in later iterations, must be put back in
    #   the mailbox at the end of the iteration
    messages = Message[]
    messages_back = Message[]

    for _ in 1:mailbox.counter[bus][reg]
        mes = @yield get(mailbox.mailbox[bus][reg])

        matching = (mailbox.local_iter_counter[bus][reg] == iter(mes))
        _decrementMessageCount(mailbox, bus, reg, matching)

        if matching 
            push!(messages, mes)
        else
            push!(messages_back, mes)
        end
    end

    if length(messages) == 0
        return(nothing, messages_back)
    elseif length(messages) == 1
        return(messages[1], messages_back)
    else
        error("More than one message matching for bus $bus from idx $idx")
    end
end
@resumable function readMessage(env::Environment, bus::Int, idx,
mailbox::MailboxManager{B2BType})
    # messages: contains messages taken into account in this iteration
    # messages_back: contains messages taken into account in later iterations, must be put back in
    #   the mailbox at the end of the iteration
    messages = Message[]
    messages_back = Message[]

    i,j,dir = idx

    for _ in 1:mailbox.counter[bus][idx]
        mes = @yield get(mailbox.mailbox[bus][idx])

        matching = (mailbox.local_iter_counter[bus][(j,i,3-dir)] == iter(mes))
        _decrementMessageCount(mailbox, bus, idx, matching)

        if matching 
            push!(messages, mes)
        else
            push!(messages_back, mes)
        end
    end

    if length(messages) == 0
        return(nothing, messages_back)
    elseif length(messages) == 1
        return(messages[1], messages_back)
    else
        error("More than one message matching for bus $bus from idx $idx")
    end
end

@resumable function retrieveBusMessages(env::Environment, 
    k::Int, j::Int, regs::Array{Int}, 
    messages_back_temp::Dict, bus_reg_iter_update::Dict,
    mailbox::MailboxManager{ErsegheType}, algoPar::BusSyncParams, lpm::LocalPowerModel{ErsegheType})

    if mailbox.ready_flag[j]        # voltageUpdate == BUSWISEMATCHING
        messages_back_reg_temp = Dict{Int, Any}()
        for reg in regs
            proc = @process readMessage(env, j, reg, mailbox)
            mes = @yield proc
            message, messages_back = mes
            if message isa Message 
                update_voltage_copy_variables!(lpm, message)
            else 
                error("BusSync: this bus must receive all messages from neighbor regions. Number of non matching messages: $(length(messages_back))")
            end
            messages_back_reg_temp[reg] = messages_back
        end
        messages_back_temp[j] = messages_back_reg_temp
        bus_reg_iter_update[j] = regs
    end
end
@resumable function retrieveBusMessages(env::Environment, 
    k::Int, j::Int, regs::Array{Int}, 
    messages_back_temp::Dict, bus_reg_iter_update::Dict,
    mailbox::MailboxManager{ErsegheType}, algoPar::AlgoParams, lpm::LocalPowerModel{ErsegheType})

    messages_back_reg_temp = Dict{Int, Any}()
    bus_reg_iter_update_temp = Int[]
    for reg in regs
        # (k==2)&&(j==65)&&println("region $reg")
        proc = @process readMessage(env, j, reg, mailbox)
        mes = @yield proc
        message, messages_back = mes
        if message isa Message 
            update_voltage_copy_variables!(lpm, message)
            push!(bus_reg_iter_update_temp, reg)
        else 
            #nothing
        end
        messages_back_reg_temp[reg] = messages_back
    end
    messages_back_temp[j] = messages_back_reg_temp
    if length(bus_reg_iter_update_temp) > 0
        bus_reg_iter_update[j] = bus_reg_iter_update_temp
        # (k==2)&&(j==65)&&println("received from regions $bus_reg_iter_update_temp")
    else
        #nothing
    end
end
@resumable function retrieveBusMessages(env::Environment, 
    k::Int, j::Int, dic_idx::Dict, 
    messages_back_temp::Dict, bus_idx_iter_update::Dict,
    mailbox::MailboxManager{B2BType}, algoPar::BusSyncParams, lpm::LocalPowerModel{B2BType})

    if mailbox.ready_flag[j]        # voltageUpdate == BUSWISEMATCHING
        messages_back_reg_temp = Dict()
        for (idx,info) in dic_idx
            i,j1,dir = idx
            proc = @process readMessage(env, j, (j1,i,3-dir), mailbox)
            mes = @yield proc
            message, messages_back = mes
            if message isa Message 
                update_voltage_copy_variables!(lpm, message)
            else 
                error("BusSync: this bus must receive all messages from neighbor regions. Number of non matching messages: $(length(messages_back))")
            end
            messages_back_reg_temp[idx] = messages_back
        end
        messages_back_temp[j] = messages_back_reg_temp
        bus_idx_iter_update[j] = Dict("buspair"=>dic_idx)
    end
end
@resumable function retrieveBusMessages(env::Environment, 
    k::Int, j::Int, dic_idx::Dict, 
    messages_back_temp::Dict, bus_idx_iter_update::Dict,
    mailbox::MailboxManager{B2BType}, algoPar::AlgoParams, lpm::LocalPowerModel{B2BType})

    messages_back_reg_temp = Dict()
    bus_idx_iter_update_temp = Dict()
    for (idx,info) in dic_idx
        i,j1,dir = idx
        proc = @process readMessage(env, j, (j1,i,3-dir), mailbox)
        mes = @yield proc
        message, messages_back = mes
        if message isa Message 
            update_voltage_copy_variables!(lpm, message)
            bus_idx_iter_update_temp[idx] = info
        else 
            #nothing
        end
        messages_back_reg_temp[idx] = messages_back
    end
    messages_back_temp[j] = messages_back_reg_temp
    if length(bus_idx_iter_update_temp) > 0
        bus_idx_iter_update[j] = Dict("buspair"=>bus_idx_iter_update_temp)
        # (k==2)&&(j==65)&&println("received from regions $bus_reg_iter_update_temp")
    else
        #nothing
    end
end

function updateLocalIterCounter(dic_counter::Dict{Int,Any}, mailbox::MailboxManager)
    # dic_counter[j][h] = i_{j k,h} or dic_counter[j][idx] = i_{idx, j}
    for (j,dic) in dic_counter
        for (h,iter) in dic
            mailbox.local_iter_counter[j][h] = iter
        end
    end
end

function updateReadyFlags(mailbox::MailboxManager, k::Int, algoPar::BusSyncParams)
    ntrig_bus = algoPar.N_trig_bus[k]

    # Update ready flags for each bus
    for (j,nmes) in countMatchingMessagesBuswise(mailbox)
        mailbox.ready_flag[j] = (nmes ≥ ntrig_bus[j])
        # println(" j $j:   $nmes ≥ $(ntrig_bus[j])")
    end
end
function updateReadyFlags(mailbox::MailboxManager, k::Int, algoPar::AlgoParams)
    # nothing
end

function checkTrigCondition(mailbox::MailboxManager, algoPar::FullSyncParams, h::Int)
    ntrig_tot = algoPar.N_trig_tot[h]
    return (countMatchingMessages(mailbox) ≥ ntrig_tot) && mailbox.init_done_flag
end
function checkTrigCondition(mailbox::MailboxManager, algoPar::BusSyncParams, h::Int)
    ntrig_bus = algoPar.N_trig_bus[h]
    ntrig_reg = algoPar.N_trig_reg[h]

    # Update ready flags for each bus
    for (j,nmes) in countMatchingMessagesBuswise(mailbox)
        mailbox.ready_flag[j] = (nmes ≥ ntrig_bus[j])
        # println("            $nmes ≥ $(ntrig_bus[j])")
    end
    return (count(values(mailbox.ready_flag)) ≥ ntrig_reg) && mailbox.init_done_flag
end
function checkTrigCondition(mailbox::MailboxManager, algoPar::AsyncParams, h::Int)
    ntrig_tot = algoPar.N_trig[h]
    return (countMatchingMessages(mailbox) ≥ ntrig_tot) && mailbox.init_done_flag
end
function checkTrigConditionInit(mailbox::MailboxManager{ErsegheType}, asyncProb::async_problem, h::Int)
    # At initialization, synchronous reception

    ntrig_bus = Dict( j => length(reg_j) for (j, reg_j) in asyncProb.duplicates_bus_list[h]["j"])
    ntrig_reg = length(asyncProb.duplicates_bus_list[h]["j"])

    # Update ready flags for each bus
    for (j,nmes) in countMatchingMessagesBuswise(mailbox)
        mailbox.ready_flag[j] = (nmes ≥ ntrig_bus[j])
    end
    return count(values(mailbox.ready_flag)) ≥ ntrig_reg
end
function checkTrigConditionInit(mailbox::MailboxManager{B2BType}, asyncProb::async_problem, h::Int)
    # At initialization, synchronous reception

    ntrig_bus = Dict( j => length(dic_j["buspair"]) for (j, dic_j) in asyncProb.duplicates_bus_list[h]["j"])
    ntrig_reg = length(asyncProb.duplicates_bus_list[h]["j"])

    # Update ready flags for each bus
    for (j,nmes) in countMatchingMessagesBuswise(mailbox)
        mailbox.ready_flag[j] = (nmes ≥ ntrig_bus[j])
    end
    return count(values(mailbox.ready_flag)) ≥ ntrig_reg
end

@resumable function sendMessage(env::Environment, k::Int, h::Int,
buses, iters, datas, delay::Number,
Mailboxes::Dict{Int,MailboxManager{ErsegheType}}, Trigs::Dict{Int,Process}, algoPar::AlgoParams)
    trig_h = Trigs[h]
    mailbox_h = Mailboxes[h]

    @yield timeout(env, delay)
    for (j,iter,data) in Iterators.zip(buses, iters, datas)
        @yield @process leaveMessage(env, k,h,j,iter,data,mailbox_h)
    end

    if checkTrigCondition(mailbox_h, algoPar, h) 
        @yield SimJulia.interrupt(trig_h)
    end
end
@resumable function sendMessage(env::Environment, k::Int, h::Int,
indexes, iters, datas, delay::Number,
Mailboxes::Dict{Int,MailboxManager{B2BType}}, Trigs::Dict{Int,Process}, algoPar::AlgoParams)
    trig_h = Trigs[h]
    mailbox_h = Mailboxes[h]

    @yield timeout(env, delay)
    for (idx,iter,data) in Iterators.zip(indexes, iters, datas)
        @yield @process leaveMessage(env, idx, k,h, iter,data,mailbox_h)
    end

    if checkTrigCondition(mailbox_h, algoPar, h) 
        @yield SimJulia.interrupt(trig_h)
    end
end
@resumable function sendMessageInit(env::Environment, k::Int, h::Int,
buses, iters, datas, delay::Number,
Mailboxes::Dict{Int,MailboxManager{ErsegheType}}, Trigs::Dict{Int,Process}, 
algoPar::AlgoParams, asyncProb::async_problem)
    trig_h = Trigs[h]
    mailbox_h = Mailboxes[h]

    @yield timeout(env, delay)
    for (j,iter,data) in Iterators.zip(buses, iters, datas)
        @yield @process leaveMessage(env, k,h,j,iter,data,mailbox_h)
    end

    if checkTrigConditionInit(mailbox_h, asyncProb, h) 
        @yield SimJulia.interrupt(trig_h)
    end
end
@resumable function sendMessageInit(env::Environment, k::Int, h::Int,
indexes, iters, datas, delay::Number,
Mailboxes::Dict{Int,MailboxManager{B2BType}}, Trigs::Dict{Int,Process}, 
algoPar::AlgoParams, asyncProb::async_problem)
    trig_h = Trigs[h]
    mailbox_h = Mailboxes[h]

    @yield timeout(env, delay)
    for (idx,iter,data) in Iterators.zip(indexes, iters, datas)
        @yield @process leaveMessage(env, idx, k,h,iter,data,mailbox_h)
    end

    if checkTrigConditionInit(mailbox_h, asyncProb, h) 
        @yield SimJulia.interrupt(trig_h)
    end
end