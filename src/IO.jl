using HDF5
using JLD, HDF5
using DataFrames
using DataStructures
using Statistics
using LightGraphs, SimpleWeightedGraphs
using PlotlyJS

import Base.show

################################################################################
# Structure Julia Data File
################################################################################

struct NotImplementedYet <: Exception end

mutable struct JLDFile
	filename::String
	filepath::String
	testcasefile::String
	df_params::DataFrame
	params::Dict{String, <:Array{T,1} where T}
	Ndone::Int64

	function JLDFile(filename::String, 
		params_dict_input::Dict{String, <:Array{<:Any,1}} = Dict{String, Array{Float64,1}}())

		@assert endswith(filename, ".jld") "'filename' must be an JLD file."
		filepath = string("results/",filename)
		testcasefile = string(filename[1:end-4], ".m")

		if isfile(filepath)
			# Read already existing values
			Memento.warn(_LOGGER, "$filename already exists.")
			jldopen(filepath, "r") do f
				Df = read(f,"param_dataframe")
				params_dict = read(f, "param_dict")
				Ndone = read(f, "Ndone")
			end

			println(filepath, " already exists.")
			ans = ""
			ans2 = ""
			while ans ∉ ["y", "n"]
				print("    Do you want to add a parameter value ? ('y'/'n'): ")
				ans = readline()
			end
			
			if ans == "y"
			# If there is a new value, ask which parameter to modify, or if there is a new parameter to add
				params_dict = params_dict_input
				params_dict, Df = addValueParam(params_dict, Df)

				# Modify JLD file
				jldopen(filepath, "r+") do f 
					delete!(f, "param_dataframe")
					delete!(f, "param_dict")

					f["param_dataframe"] = Df
					f["param_dict"] = params_dict
				end
			end
		else
			Df = initDfparams(params_dict_input)
			params_dict = params_dict_input
			Ndone = 0
		end
		new(filename, filepath, testcasefile, Df, params_dict, Ndone)
	end

	function JLDFile(filename::String, filepath::String, testcasefile::String)
		# For reading only
		@assert endswith(filepath, filename) "Filepath must end with filename."
		@assert isfile(filepath) "Filepath not valid."

		Df = DataFrame()
		params_dict = Dict()
		fid = jldopen(filepath, "r")
			Df = read(fid,"param_dataframe")
			params_dict = read(fid, "param_dict")
		close(fid)

		Ndone =0
		new(filename, filepath, testcasefile, Df, params_dict, Ndone)
	end
end

function show(io::IO, file::JLDFile)
	println("JLDFile ", file.filename, " :")
	for (k,v) in file.params
		println("	", k, " : ", length(v))
	end
end

function initDfparams(params_dict::Dict{String, <:Array{T,1} where T})
	# Create a dataframe that lists all points from the current study.
	# The study is meshing every parameter value with every other parameter value. 
	# Each point is given a number ID that can be retrieved using the findNID function.
	Df = DataFrame()
	for (k,v) in params_dict
		df = DataFrame(Symbol(k)=>v)
		if size(Df,2) == 0
			Df = df
		else
			Df = crossjoin(df, Df)
		end
	end

	Npoints = size(Df, 1)
	Df.NID = range(1,stop=Npoints)

	Df
end

function addValueParam(params_dict_input::Dict{String, <:Array{T,1} where T}, 
	params_df_input::DataFrame )
	# Adds value to already existing parameter.

	params_dict_output = copy(params_dict_input)
	new_params_dict = copy(params_dict_input)	# Used to update the dataframe

	# Adds values to the dataframe
	new_df = initDfparams(new_params_dict)

	Npoints_old = size(params_df_input, 1)
	Npoints_new = size(new_df, 1)

	new_df.NID = range(Npoints_old+1, stop = Npoints_old+Npoints_new)
	params_df_output = vcat(params_df_input, new_df)

	# Returns the updated dictionary and dataframe
	return params_dict_output, params_df_output
end

function computingQueue(file::JLDFile)
	Df = DataFrame()

	if !isfile(file.filepath)
		out = file.df_params
	else
		jldopen(file.filepath, "r") do f
			Df = read(f,"param_dataframe")
			params_dict = read(f, "param_dict")
			Ndone = read(f, "Ndone")
		end

		out = DataFrame()
		for rdf in eachrow(Df)
			nid = rdf.NID
			jldopen(file.filepath, "r") do f
				if !exists(f,string(nid))
					push!(out, rdf)
				end
			end
		end
	end
	out
end

################################################################################
# File creation and modification
################################################################################

function createfile(file::JLDFile)
	@assert !isfile(file.filepath) "File already exists !"

	jldopen(file.filepath, "w", compress=true) do f 
		f["param_dataframe"] = file.df_params
		f["param_dict"] = file.params
		f["Ndone"] = 0
	end

	println("	", file.filename, " has been successfully created.")
end

function addpoint(file::JLDFile, NID::Int, monitor::Dict)
	if !isfile(file.filepath)
		createfile(file)
	end

	jldopen(file.filepath, "r+", compress=true) do f
		f[string(NID)] = monitor

		# Updating counter Ndone
		Ndone = read(f,"Ndone")
		if NID > Ndone
			delete!(f, "Ndone")
			f["Ndone"] = NID
			file.Ndone = NID
		end
	end
end

################################################################################
# Find NIDs of given parameter values
################################################################################

function findNID(file::JLDFile, point::Dict{String, <:Number})
	df = file.df_params
	for (k,v) in point
		df = df[ df[:,Symbol(k)].== v ,:]
	end
	NID = df[:,:NID]

	return NID, df
end

################################################################################
# Read raw results
################################################################################
function readRes(file::JLDFile, df_NID::DataFrame=DataFrame())
	df = (size(df_NID,1) == 0) ? file.df_params : df_NID
	dict_out = Dict{Int64, Any}()
	jldopen(file.filepath, "r") do f 
		for idrow in 1:size(df, 1)
			nid = df.NID[idrow]
			if exists(f, string(nid))
				dict_out[nid] = read(f, string(nid))
    		end
		end
	end

	dict_out
end

################################################################################
# Plotting functions
################################################################################

function plotComparison(file::JLDFile, arg::String="voltage_comp")
	@assert arg ∈ ["voltage_comp", "gen_comp", "branch_comp", "vanish"] "Unvalid argument."
	df_NID = file.df_params
	dic_res = readRes(file, df_NID)

	plotComparison(dic_res, df_NID, arg)
end
function plotComparison(dic_res::Dict, df_NID::DataFrame, arg::String="voltage_comp")
	deltas = [0.1,0.2,0.3,0.4,0.5,0.6,0.7,0.8,0.9,1.0]
	df_nid = df_NID[map(x-> x∈deltas, df_NID.delta),:]
	sort!(df_nid, :delta)

	data = GenericTrace[]
	for dfrow in eachrow(df_nid)
		nid = dfrow.NID
		delta = dfrow.delta
		if nid in keys(dic_res)
			dic = dic_res[nid]
			res = sum(v[arg] for (k,v) in dic["data"]["region"] if length(v[arg])>1)
			s = log10.(max.(zeros(length(res)),res))
			trace = scatter(;	y=s,
								name = delta)
			push!(data,trace)
		end
	end
	PlotlyJS.plot(data)
end

function plotConvergenceTime(file::JLDFile, E_abs2::Number; arg::String="voltage_comp")
	df_sync = convergenceIterationSync(file, E_abs2, arg)
	norm = mean(df_sync.conv_time)
	df, norm = plotConvergenceTimeAbstract(file, E_abs2, norm; arg)
	plotConvergenceTimeAbstract(df,norm)
end
function plotConvergenceTime(file::JLDFile, E_abs2::Number, norm::Number; arg::String="voltage_comp")
	df_res = convergenceIteration(file, E_abs2, arg)
	PlotlyJS.plot(df_res.delta, df_res.conv_time./norm, mode = "markers")
end
function plotConvergenceTime(dic_res::Dict, df_nid::DataFrame, E_abs2::Number, norm::Number; arg::String="voltage_comp")
	df_res = convergenceIteration(dic_res, df_nid, E_abs2, arg)
	data = scatter(;x=df_res.delta, y=df_res.conv_time./norm, mode = "markers")
	layout = Layout(xaxis_title = "δ", yaxis_title="Normalized convergence time", 
					width = 600, height = 500)
	PlotlyJS.plot(data, layout)
end

function plotConvergenceTimeAbstract(file::JLDFile, E_abs2::Number, norm::Number; arg::String="voltage_comp")
	df_res = convergenceIteration(file, E_abs2, arg)
	gd = groupby(df_res, [:alpha, :beta, :sigma, :delta])
	df = combine(gd, :conv_time => (x->[(mean(x), std(x))] ) => [:convtimemean, :convtimestd])

	return df, norm
end
function plotConvergenceTimeAbstract(df::DataFrame, norm::Number)
	data = GenericTrace[]

	trace = PlotlyJS.scatter(;x = df.delta.*100, y=df.convtimemean./norm, mode = "markers")
	push!(data, trace)

	if "convtimestd"∈names(df)
		sort!(df, :delta)		
		trace = PlotlyJS.scatter(;x=df.delta.*100, y=(df.convtimemean.+df.convtimestd)./norm, mode="lines", line_width=0.2, line_color = "green")
		push!(data, trace)
		trace = PlotlyJS.scatter(;x=df.delta.*100, y=(df.convtimemean.-df.convtimestd)./norm, mode="lines", line_width=0.2, line_color = "red")
		push!(data, trace)
	end

	layout = PlotlyJS.Layout(
				#width = 334,
				#height = 110,
				margin = attr(
				               l = 2,
				               r = 5,
				               t = 2,
				               b = 30,
				               ),
				showlegend = false,
				legend = attr(
				               x = 0.7,
				               y = 0.1,
				               font_size = 10,
				               font_family = "PT Sans Narrow",
				   	),
				title = attr(
                         text = "Normalized convergence time",
                         font_family = "PT Sans Narrow",
                         font_size = 12,
                         y = 0.9,
             		),
				xaxis = attr(
				               #constraintowards = "bottom",
				               #anchor = "y2",
				               #type = "log",
				               #range = [-4,1],
								automargin = true,
				               title = attr(
				                               text = "Proportion of received messages before update (%)",
				                               standoff = 0,
				                               font_size = 10,
				                               font_color = "#444444",
				                               font_family = "PT Sans Narrow",
				                   ), 
				               tickfont_size = 10,
				               tickfont_family = "PT Sans Narrow",
				               zeroline=true,
				               ticks = "inside",
				               dtick = 10,
				               showline = true,
				               linecolor= "black",
				               linewidth= 0.5,
				               mirror= "allticks",
				               ),
				yaxis = attr(   
				               #domain = [0.65,1],
								#title = attr(
								#              #standoff = 80,
								#              text = "Normalized convergence time (%)", 
								#              font_size = 10,
								#              font_color = "#444444"
								#  ), 
				               # color = "#1f77b4",
				               tickfont_size = 10,
				               tickfont_family = "PT Sans Narrow",
				               # tickfont = attr(
				               #             color="#1f77b4"
				               #         ),
				               automargin = true,
				               zeroline=true,
				               #dtick = 0.2,
				               ticks = "inside",
				               showline = true,
				               linecolor= "black",
				               linewidth= 0.5,
				               mirror= "allticks",
				               ),
				#annotations = [
				#               attr(
				#                       visible = true,
				#                       text = "Global cost value C<sub>δ</sub>",
				#                       font = attr( 
				#                           size = 11, 
				#                           # color = "black", 
				#                           family = "PT Sans Narrow"),
				#                       showarrow = false,
				#                       yref = "y",
				#                       x = -1.5,
				#                       y = -72500,
				#                   ),
				#               attr(
				#                       visible = true,
				#                       text = "Trades comparison v<sub>δ</sub>",
				#                       font = attr( 
				#                           size = 11, 
				#                           # color = "black", 
				#                           family = "PT Sans Narrow"),
				#                       showarrow = false,
				#                       yref = "y2",
				#                       x = -1.5,
				#                       y = 0.23,
				#                   ),
				#           ],
				)
	PlotlyJS.plot(data, layout)
end
function plotConvergenceTimeComparison(norm::Number=1)
	#df_res_0_1 = load("results/case118_erseghe_sigma0_1_tirages_mean.jld", "df_res")
	df_res_0_3 = load("results/case118_erseghe_sigma0_3_tirages_mean.jld", "df_res")
	df_res_0_3_2 = load("results/case118_erseghe_sigma0_3_tirages_2_mean.jld", "df_res")
	df_res_0_3_3 = load("results/case118_erseghe_sigma0_3_tirages_3_mean.jld", "df_res")
	df_res_0_5 = load("results/case118_erseghe_sigma0_5_tirages_mean.jld", "df_res")
	df_res_0_7 = load("results/case118_erseghe_sigma0_7_tirages_mean.jld", "df_res")
	#df_res_0_9 = load("results/case118_erseghe_sigma0_9_tirages_mean.jld", "df_res")
	df_res_1_0 = load("results/case118_erseghe_sigma1_0_tirages_mean.jld", "df_res")

	#norm_0_1 = load("results/case118_erseghe_sigma0_1_tirages_mean.jld", "norm")
	norm_0_3 = load("results/case118_erseghe_sigma0_3_tirages_mean.jld", "norm")
	norm_0_3_2 = load("results/case118_erseghe_sigma0_3_tirages_2_mean.jld", "norm")
	norm_0_3_3 = load("results/case118_erseghe_sigma0_3_tirages_3_mean.jld", "norm")
	norm_0_5 = load("results/case118_erseghe_sigma0_5_tirages_mean.jld", "norm")
	norm_0_7 = load("results/case118_erseghe_sigma0_7_tirages_mean.jld", "norm")
	#norm_0_9 = load("results/case118_erseghe_sigma0_9_tirages_mean.jld", "norm")
	norm_1_0 = load("results/case118_erseghe_sigma1_0_tirages_mean.jld", "norm")

	#df_res_0_1.norm = norm_0_1.*ones(size(df_res_0_1,1))
	df_res_0_3.norm = norm_0_3.*ones(size(df_res_0_3,1))
	df_res_0_3_2.norm = norm_0_3_2.*ones(size(df_res_0_3_2,1))
	df_res_0_3_3.norm = norm_0_3_3.*ones(size(df_res_0_3_3,1))
	df_res_0_5.norm = norm_0_5.*ones(size(df_res_0_5,1))
	df_res_0_7.norm = norm_0_7.*ones(size(df_res_0_7,1))
	#df_res_0_9.norm = norm_0_9.*ones(size(df_res_0_9,1))
	df_res_1_0.norm = norm_1_0.*ones(size(df_res_1_0,1))

	df_res = vcat(
					#df_res_0_1,
					df_res_0_3,
					df_res_0_3_2,
					df_res_0_3_3,
					df_res_0_5,
					df_res_0_7,
					#df_res_0_9,
					df_res_1_0)

	data = GenericTrace[]
	for gdf in groupby(df_res, :sigma)
		println("sigma:$(gdf.sigma[1])")
		trace = PlotlyJS.scatter(;
						x = gdf.delta.*100,
						y = gdf.convtimemean,
						name = string(gdf.sigma[1]),
						mode = "markers"
						)
		push!(data, trace)
	end
	layout = PlotlyJS.Layout(
				#width = 334,
				#height = 110,
				margin = attr(
				               l = 2,
				               r = 5,
				               t = 30,
				               b = 30,
				               ),
				showlegend = true,
				legend = attr(
				               #x = 0.7,
				               #y = 0.1,
				               #font_size = 10,
				               font_family = "PT Sans Narrow",
				   	),
				title = attr(
                         text = "Normalized convergence time",
                         font_family = "PT Sans Narrow",
                         #font_size = 12,
                         #y = 0.9,
             		),
				xaxis = attr(
				               #constraintowards = "bottom",
				               #anchor = "y2",
				               #type = "log",
				               #range = [-4,1],
								automargin = true,
				               title = attr(
				                               text = "Proportion of received messages before update (%)",
				                               standoff = 0,
				                               #font_size = 10,
				                               font_color = "#444444",
				                               font_family = "PT Sans Narrow",
				                   ), 
				               #tickfont_size = 10,
				               tickfont_family = "PT Sans Narrow",
				               zeroline=true,
				               ticks = "inside",
				               dtick = 10,
				               showline = true,
				               linecolor= "black",
				               linewidth= 0.5,
				               mirror= "allticks",
				               ),
				yaxis = attr(   
				               #domain = [0.65,1],
								#title = attr(
								#              #standoff = 80,
								#              text = "Normalized convergence time (%)", 
								#              font_size = 10,
								#              font_color = "#444444"
								#  ), 
				               # color = "#1f77b4",
				               #tickfont_size = 10,
				               tickfont_family = "PT Sans Narrow",
				               # tickfont = attr(
				               #             color="#1f77b4"
				               #         ),
				               automargin = true,
				               zeroline=true,
				               #dtick = 0.2,
				               ticks = "inside",
				               showline = true,
				               linecolor= "black",
				               linewidth= 0.5,
				               mirror= "allticks",
				               ),
				)
	PlotlyJS.plot(data, layout)
end

# function plotVanish(file::JLDFile)
# 	df_NID = file.df_params
# 	dic_res = readRes(file, df_NID)

# 	deltas = [0.1,0.2,0.3,0.4,0.5,0.6,0.7,0.8,0.9]
# 	df_nid = df_NID[map(x-> x∈deltas, df_NID.delta),:]
# 	sort!(df_nid, :delta)

# 	data = GenericTrace[]
# 	for dfrow in eachrow(df_nid)
# 		nid = dfrow.NID
# 		delta = dfrow.delta
# 		if nid in keys(dic_res)
# 			dic = dic_res[nid]
# 			s = log10.(sum(v[arg] for (k,v) in dic["data"]["region"]))
# 			trace = scatter(;	y=s,
# 								name = delta)
# 			push!(data,trace)
# 		end
# 	end
# 	PlotlyJS.plot(data)
# end


################################################################################
# Find convergence and read result at convergence
################################################################################

function convergenceIterationSync(file::JLDFile, E_abs2::Number, arg::String="voltage_comp")
	df_nid = file.df_params
	df = df_nid[df_nid.delta .== 1.0, :]

	(size(df, 1) != 1) && Memento.warn(_LOGGER, "Multiple synchronous points, taking the mean value.")
	dic_res = readRes(file, df)
	convergenceIteration(dic_res, df, E_abs2, arg)
end

function convergenceIteration(file::JLDFile, E_abs2::Number, arg::String="voltage_comp")
	df_nid = file.df_params
	dic_res = readRes(file, df_nid)
	convergenceIteration(dic_res, df_nid, E_abs2, arg)
end
function convergenceIteration(dic_res::Dict, df_nid::DataFrame, E_abs2::Number, arg::String="voltage_comp")
	@assert arg ∈ ["voltage_comp", "gen_comp", "branch_comp", "vanish"] "Unvalid argument."
	N = size(df_nid,1)
	df_nid.conv_time = zeros(N)
	df_nid.conv_idx = zeros(Int, N)

	for dfrow in eachrow(df_nid)
		nid = dfrow.NID
		delta = dfrow.delta
		if nid in keys(dic_res)
			dic = dic_res[nid]
			res = sum(v[arg] for (k,v) in dic["data"]["region"] if length(v[arg])>1)
			conv_iter = convergenceIteration(res, E_abs2)

			δt = dfrow.sampling_period
			dfrow.conv_time = conv_iter*δt
			dfrow.conv_idx = conv_iter
		end
	end

	df_nid
end
function convergenceIteration(array::Array{<:Number,1}, E_abs2::Number)
	rmax, kmin = findmax(array)
	test = array .- E_abs2

	if kmin == length(test)
		conv_iter = kmin
		Memento.warn(_LOGGER, "Could not find the convergence iteration.")
	else
		try
			# conv_iter = minimum(findall(x->(x<0), test[kmin:end]))  + (kmin-1)
			conv_iter = maximum(findall(x->(x>0), test[kmin:end]))  + (kmin-1)
			if conv_iter == length(test)
				Memento.warn(_LOGGER, "Could not find the convergence iteration.")
			end
		catch
			conv_iter = length(test)
			Memento.warn(_LOGGER, "Could not find the convergence iteration.")
		end
	end
	return conv_iter
end

# function resAtConvergence(array::Union{Array{<:Number}, <:Number}, conv_iter::Int64)
# 	if typeof(array)<: Array{<:Number,3}
# 		out = array[:,:,conv_iter]
# 	elseif typeof(array)<: Array{<:Number,2}
# 		out = array[:,conv_iter]
# 	elseif typeof(array)<: Array{<:Number,1}
# 		out = array[conv_iter]
# 	elseif typeof(array)<:Number
# 		out = array
# 	else
# 		throw(NotAnArray())
# 	end
# 	return out
# end

# function readAtConvergence(file::JLDFile, df_NID::DataFrame, args::Array{String,1}, 
# 	Eprim_abs2::Number=1e-3)

# 	new_args = unique(vcat(args, "primal_res"))

# 	resDictIn = readRes(file, df_NID, new_args, 
# 		x->convergenceIteration(x, Eprim_abs2), "primal_res", "conv_iter")

# 	resDictOut = Dict{Int64, Dict{String, Any}}()
# 	for (k,v) in resDictIn
# 		conv_iter = v["conv_iter"]
# 		d = Dict{String, Any}()
# 		for (arg, value) in v
# 			d[arg] = resAtConvergence(value, conv_iter)
# 		end
# 		resDictOut[k] = d
# 	end		

# 	resDictOut
# end

# function flattenResults(file::JLDFile, df_NID::DataFrame, 
# 	dict_in::Dict{Int64, <:Dict{String, <:Any}}, pn::power_network,mem::memoryArrays)
# 	# Create dataframe with scalar results for each point in df_NID.

# 	function flattenResults(array::Union{Array{<:Number}, <:Number}, 
# 		arg::String, pn::power_network, mem::memoryArrays)

# 		if arg == "trades"
# 			# trade residual sum(|tij+tji|)
# 			output = sum(array)
# 			outputname = "trade_residual"
# 		elseif arg == "prices"
# 			# prices mean for active trades
# 			N_notnull = sum(array.≠0)
# 			output = sum(array)/N_notnull
# 			outputname = "mean_price"
# 		elseif arg == "primal_res"
# 			# sum of local residuals
# 			output = sum(array)
# 			outputname = "primal_res"
# 		elseif arg == "dual_res"
# 			# sum of local residuals
# 			output = sum(array)
# 			outputname = "dual_res"
# 		elseif arg == "length_mailbox"
# 			# mean of local length_mailbox
# 			output = sum(array)/length(array)
# 			outputname = "length_mailbox"
# 		elseif arg == "social_welfare"
# 			# already a scalar
# 			output = array
# 			outputname = "social_welfare"
# 		elseif arg == "message"
# 			# sum of all messages
# 			output = sum(array)
# 			outputname = "sum_messages"
# 		elseif arg == "power"
# 			# total power produced
# 			output = sum((array-mem.power).^2)
# 			outputname = "COUT_PUISSANCE"
# 		elseif arg == "computation_time"
# 			output = array
# 			outputname = "simulation_time"
# 		elseif arg == "conv_iter"
# 			output = array
# 			outputname = "conv_iter"
# 		elseif arg == "compare_trades"
# 			output = array
# 			outputname = "compare_trades"
# 		else
# 			print(arg)
# 		end

# 		return output, outputname
# 	end

# 	inputkeys = collect(keys(dict_in[collect(keys(dict_in))[1]]))

# 	flatOutputNames = Dict(
# 							"trades" => "trade_residual",
# 							"prices" => "mean_price",
# 							"primal_res" => "primal_res",
# 							"dual_res" => "dual_res",
# 							"length_mailbox" => "length_mailbox",
# 							"social_welfare" => "social_welfare", 
# 							"message" =>  "sum_messages", 
# 							"power" => "COUT_PUISSANCE",
# 							"computation_time" => "simulation_time",
# 							"conv_iter" => "conv_iter",
# 							"compare_trades" => "compare_trades"
# 							)
# 	outputkeys = [flatOutputNames[inkey] for inkey in inputkeys]

# 	Npoints = size(df_NID,1)
# 	Df = DataFrame( Dict(outputkeys[i]=> zeros(Npoints) for i=1:length(outputkeys)) )
# 	Df.NID = 1:Npoints 		# temporary values

# 	k = 1
# 	for (NID, dict) in dict_in
# 		Df[k, :NID] = NID
# 		for (arg, res_arg) in dict
# 			arg2 = flatOutputNames[arg]
# 			Df[k, Symbol(arg2)], name = flattenResults(res_arg, arg, pn, mem)
# 		end
# 		k = k+1
# 	end

# 	join(df_NID, Df, on= :NID)
# end

# function flattenResultsTradewise(file::JLDFile, df_NID::DataFrame,
# 	dict_in::Dict{Int64, <:Dict{String, <:Any}}, 
# 	pn::power_network)

# 	function flattenResultsTradewise( i::Int64, j::Int64,
# 		array::Union{Array{<:Number}, <:Number}, 
# 		arg::String, pn::power_network)

# 		if arg == "trades"
# 			# trade residual sum(|tij+tji|)
# 			output = (array[i,j] + array[j,i])^2
# 			output = array[i,j]
# 			# println("   Warning: 'trade_res' is equal to the actual trade.")
# 			outputname = "trade_res"
# 		elseif arg == "prices"
# 			# prices mean for active trades
# 			output = array[i,j]
# 			outputname = "price"
# 		elseif arg == "message"
# 			# sum of all messages
# 			output = array[i,j]
# 			outputname = "messages"
# 		elseif arg == "conv_iter"
# 			output = array
# 			outputname = "conv_iter"
# 		else
# 			print(arg)
# 		end

# 		return output, outputname
# 	end

# 	comm_graph = pn.comm_graph
# 	Ω = pn.Ω
# 	NΩ = length(Ω)

# 	inputkeys = collect(keys(dict_in[collect(keys(dict_in))[1]]))

# 	flatOutputNames = Dict(
# 							"trades" => "trade_res",
# 							"prices" => "price",
# 							"message" =>  "messages", 
# 							"conv_iter" => "conv_iter",
# 							)
# 	outputkeys = [flatOutputNames[inkey] for inkey in inputkeys if (inkey ≠ "primal_res")]
# 	outputkeys = vcat(outputkeys, ["i", "j", "distance"])

# 	Npoints = size(df_NID,1)*ne(comm_graph)*2
# 	Df = DataFrame( Dict(outputkeys[i]=> zeros(Npoints) for i=1:length(outputkeys)) )
# 	Df.NID = 1:Npoints 		# temporary values

# 	k = 1
# 	for (NID, dict) in dict_in
# 		for (i,j) in Iterators.product(1:NΩ, 1:NΩ)
# 			!(has_edge(comm_graph, i, j)) && continue
# 			for (arg, res_arg) in dict
# 				(arg == "primal_res") && continue

#     			arg2 = flatOutputNames[arg]
#     			Df[k, Symbol(arg2)], name = flattenResultsTradewise(i, j, res_arg, arg, pn)
#     			Df[k, :i] = i
#     			Df[k, :j] = j

#     			Df[k, :distance] = comm_graph.weights[i,j]
# 			end
# 		Df[k, :NID] = NID
# 		k = k+1
# 		end    		
# 	end

# 	join(df_NID, Df, on= :NID, kind=:inner)
# end

# ################################################################################
# # Dataframe to plot results
# ################################################################################

# function DfSweepConvergence(file::JLDFile, Eprim_rel2::Number, mem::memoryArrays) 
# 	df_params = file.df_params
# 	DfSweepConvergence(file, df_params, Eprim_rel2, mem)
# end

# function DfSweepConvergence(file::JLDFile, df_params::DataFrame, Eprim_abs2::Number, mem::memoryArrays) 

# 	# # Eprim_abs2
# 	par = power_network(file.testcasefile, "full_P2P")
# 	# pmin = par.Pmin
# 	# pmax = par.Pmax
# 	# optimal_power_exchange = sum(max.(pmin.^2, pmax.^2))		# Needed to compute absolute Epsilon_prim
# 	# Eprim_abs2 = Eprim_rel2*optimal_power_exchange

# 	#
# 	resDictOut = readAtConvergence(file, df_params, 
# 		["primal_res", "trades", "power", "social_welfare", "prices", "message"], 
# 		Eprim_abs2)

# 	Df = flattenResults(file, df_params, resDictOut, par, mem)
# 	Df.conv_time = Df.conv_iter.*Df.sampling_period

# 	Df
# end

# function DfCompareTrades(file::JLDFile, Eprim_rel2::Number, ref_point::Dict{String, <:Number})

# 	# Compares trades, point by point between ref_point and other points
# 	# the reference point changes with gamma

# 	function compareTrades(Dict1::Dict{String, <:Any}, Dict2::Dict{String, <:Any})
# 		T1 = Dict1["trades"]
# 		T2 = Dict2["trades"]

# 		sqrt(sum((T1-T2).^2))
# 	end

# 	function compareTrades2(Dict1::Dict{String, <:Any}, Dict2::Dict{String, <:Any})
# 		T1 = Dict1["trades"]
# 		T2 = Dict2["trades"]

# 		ans = sum(abs.(T1[1:30,31:110]-T2[1:30,31:110]))
# 		# println(ans)

# 		ans
# 	end

# 	function compareTrades(refDf::DataFrame, df_params::DataFrame, 
# 		resDict::Dict{Int64, <:Dict{String, <:Any}})
# 		for (k,v) in resDict
# 			# refDict with same gamma value
# 			gamma = df_params[df_params.NID.==k, :gamma][1]
# 			refNID = refDf[refDf.gamma.==gamma, :NID][1]
# 			refDict = resDict[refNID]
# 			v["compare_trades"] = compareTrades2(refDict, v)
# 		end
# 		resDict
# 	end

# 	# Eprim_abs2
# 	par = power_network(file.testcasefile, "full_P2P")
# 	pmin = par.Pmin
# 	pmax = par.Pmax
# 	optimal_power_exchange = sum(max.(pmin.^2, pmax.^2))		# Needed to compute absolute Epsilon_prim
# 	Eprim_abs2 = Eprim_rel2*optimal_power_exchange

# 	# find NID of reference point
# 	NIDs_ref, df = findNID(file, ref_point)

# 	resDict = readAtConvergence(file, file.df_params, 
# 		["primal_res", "trades", "power"], 
# 		Eprim_abs2)

# 	compareTrades(df, file.df_params, resDict)

# 	Df = flattenResults(file, file.df_params, resDict, par)
# 	Df.conv_time = Df.conv_iter.*Df.sampling_period

# 	Df
# end

# function DfSweepTradewiseConvergence(file::JLDFile, Eprim_rel2::Number, df::DataFrame) 

# 	# Eprim_abs2
# 	par = power_network(file.testcasefile, "full_P2P")
# 	pmin = par.Pmin
# 	pmax = par.Pmax
# 	optimal_power_exchange = sum(max.(pmin.^2, pmax.^2))		# Needed to compute absolute Epsilon_prim
# 	Eprim_abs2 = Eprim_rel2*optimal_power_exchange

# 	#
# 	df_params = file.df_params
# 	resDictOut = readAtConvergence(file, df, 
# 		["trades", "prices", "message"], 
# 		Eprim_abs2)

# 	#
# 	Df = flattenResultsTradewise(file, df, resDictOut, par)
# end

# ################################################################################
# # Dividing data to compute
# ################################################################################
#     ### Avoid out of memory error
#     ### Creates a jld file that contains the Dataframes results

# function divideAndCompute_SweepConvergence(JLDfile::JLDFile, filepath::String, 
# 	Eprim_rel2::Number, mem::memoryArrays, tirages_bool::Bool)

# 	@assert endswith(filepath, ".jld") "Filepath must end with '.jld'."

# 	DF = JLDfile.df_params

# 	# Creates the jld output file if it doesn't exist
# 	if !isfile(filepath)
# 		fid = jldopen(filepath, "w")
# 		close(fid)
# 	end

# 	# Grouping by delta
# 	if tirages_bool 
#     	for df in groupby(DF, [:tirages, :delta])
#     		tirages = df.tirages[1]
#     		delta = df.delta[1]
#     		print("Processing tirages = ", tirages, "           delta=",delta)
#     		sub_df = DfSweepConvergence(JLDfile, DataFrame(df), Eprim_rel2, mem)
#     		jldopen(filepath, "r+") do fid 
#     			write(fid, string("tirages", tirages,"_delta",delta), sub_df)
#     		end

#     		println("... done")
#     	end
#     else
#     	for df in groupby(DF, [:delta])
#     		# tirages = df.tirages[1]
#     		delta = df.delta[1]
#     		print("           delta=",delta)
#     		sub_df = DfSweepConvergence(JLDfile, DataFrame(df), Eprim_rel2, mem)
#     		jldopen(filepath, "r+") do fid 
#     			write(fid, string("_delta",delta), sub_df)
#     		end

#     		println("... done")
#     	end
#     end
# end

# ################################################################################
# # Read data already stored at convergence
# ################################################################################

# function readAtConvergence(filepath::String)
# 	@assert isfile(filepath) "Incorrect filepath."
# 	@assert endswith(filepath, ".jld") "File must be Julia Data type: .jld"

# 	fid = jldopen(filepath, "r") 
# 		Df = read(fid, "param_dataframe")
# 		N = size(Df,1)
# 		Df[:,:conv_time] = Df.sampling_period

# 		for i in 1:N
# 			Df[i,:conv_time] = Df[i,:conv_time]*read(fid, string(Df.NID[i], "/conv_iter"))
# 		end
# 	close(fid)
	
# 	Df
# end
