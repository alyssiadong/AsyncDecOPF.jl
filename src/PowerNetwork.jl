using CSV
using LightGraphs, SimpleWeightedGraphs
using DataFrames
using Ipopt
using DecentralizedPowerModels
import DecentralizedPowerModels: ProblemType
using Gadfly, LightGraphs, SimpleWeightedGraphs, GraphPlot, Colors

### Parameters' structure
struct async_problem{T<:ProblemType}
    comm_graph_region::SimpleWeightedGraph{Int64}   # Communication graph 
                                                    # between regions
    duplicates_bus_list::Dict

    function async_problem(ds::DecentralizedSystem{<:ErsegheType})
        g = SimpleWeightedGraph(length(ds.pms))     # comm_graph_region
        duplicates = Dict{Int, Any}()               # duplicates_bus_list
        for (nreg, lpm) in ds.pms
            reg_dic = Dict{Int,Any}()
            pm = lpm.pm
            neighbor_regions = Set{Int64}()
            for (nbus, bus) in PowerModels.ref(pm,:bus)
                push!(neighbor_regions, bus["duplicated_regions"]...)
                if length(bus["duplicated_regions"]) > 1
                    temp = Set(bus["duplicated_regions"])
                    temp = setdiff(temp, nreg)
                    reg_dic[nbus] = collect(temp)
                end
            end
            setdiff!(neighbor_regions, nreg)
            duplicates[nreg] = Dict("j"=>reg_dic)

            for neig_reg in neighbor_regions
                if !has_edge(g, nreg, neig_reg)
                    loc_a = [
                            PowerModels.ref(pm, :region, nreg, "loc_x"),
                            PowerModels.ref(pm, :region, nreg, "loc_y")
                            ]
                    loc_b = [
                            PowerModels.ref(ds.pms[neig_reg].pm, :region, neig_reg, "loc_x"),
                            PowerModels.ref(ds.pms[neig_reg].pm, :region, neig_reg, "loc_y")
                            ]
                    # adding edges to communication graph
                    add_edge!(g, nreg, neig_reg, distance(loc_a, loc_b))
                end
            end
        end

        new{ErsegheType}(g, duplicates)
    end

    function async_problem(ds::DecentralizedSystem{<:B2BType})
        g = SimpleWeightedGraph(length(ds.pms))     # comm_graph_region
        duplicates = Dict{Int, Any}()               # duplicates_bus_list
        for (nreg, lpm) in ds.pms
            reg_dic = Dict{Int,Any}()
            pm = lpm.pm
            neighbor_regions = Set{Int64}()
            for nbus in PowerModels.ids(pm,:bus)
                bus_dic = Dict()
                for (idx,info) in PowerModels.ref(pm, :region, nreg, "out_buspairs_collection")
                    i,j,dir = idx
                    I = (i,j)[dir]
                    if (I == nbus) 
                        bus_dic[idx] = deepcopy(info)
                    end
                    push!(neighbor_regions, info["connected_region"])
                end
                if length(bus_dic) ≠ 0
                    reg_dic[nbus] = Dict("buspair"=>bus_dic)
                end
            end
            duplicates[nreg] = Dict("j"=>reg_dic)
            for neig_reg in neighbor_regions
                if !has_edge(g, nreg, neig_reg)
                    loc_a = [
                            PowerModels.ref(pm, :region, nreg, "loc_x"),
                            PowerModels.ref(pm, :region, nreg, "loc_y")
                            ]
                    loc_b = [
                            PowerModels.ref(ds.pms[neig_reg].pm, :region, neig_reg, "loc_x"),
                            PowerModels.ref(ds.pms[neig_reg].pm, :region, neig_reg, "loc_y")
                            ]
                    # adding edges to communication graph
                    dist = distance(loc_a, loc_b)
                    (dist == 0.0) && Memento.warn(_LOGGER,"Distance is null. The edge is not added in communication graph.")
                    add_edge!(g, nreg, neig_reg, dist)
                end
            end
        end

        new{B2BType}(g, duplicates)
    end
end

### Distance computation
function distance(loc_a::Array{Float64}, loc_b::Array{Float64})
    ### Computes the euclidian distance between loc_a = [xa, ya]
    ### and loc_b = [xb, yb].
    return sqrt((loc_a[1]-loc_b[1])^2 + (loc_a[2]-loc_b[2])^2)
end

### Optimal result using PowerModels
function optimal_res(file::String)
    ipopt_solver = optimizer_with_attributes(Ipopt.Optimizer, 
        "tol"=>1e-8, "print_level"=>2, "constr_viol_tol"=>1e-6)
    result_ac_opf = run_ac_opf(file, ipopt_solver)
end

### Plot power network
function plot_network(ds::DecentralizedSystem)

    # Listing every buses and their respective regions
    regions_buses = Dict{Int,Any}()
    for (i,pm) in ds.pms
        regions_buses[i] = PowerModels.ref(pm,:region,i,"bus") 
    end

    Nbuses = sum(length(region_buses) for (reg,region_buses) in regions_buses)
    g = SimpleGraph(Nbuses)

    # Creating a dictionnary to easily find the region of each bus
    buses = vcat(values(regions_buses)...)
    buses_region = Dict{Int,Int}()
    for bus in buses
        for (reg,region_buses) in regions_buses
            if bus ∈ region_buses
                buses_region[bus] = reg
            end
        end
    end

    # Adding edges
    for (reg,pm) in ds.pms
        for (l,i,j) in PowerModels.ref(pm,:region,reg,"arcs_from")
            I = findfirst(x->(x==i),buses)
            J = findfirst(x->(x==j),buses)
            add_edge!(g, I, J)
        end
    end

    # Colors
    colors = distinguishable_colors(length(regions_buses);
                     lchoices=range(100, stop=255, length=15))
    colors_dict = Dict(collect(keys(regions_buses))[i] => colors[i] for i in 1:length(regions_buses))
    colors_bus = [colors_dict[buses_region[bus]] for bus in buses]

    gplot(g, nodelabel=buses, nodefillc=colors_bus)
end