using Distributed
# #addprocs(3)

@everywhere using Pkg
@everywhere Pkg.activate(".")

@everywhere using InfrastructureModels, PowerModels, DecentralizedPowerModels
@everywhere import DecentralizedPowerModels:build_dec_opf_erseghe, build_dec_opf_b2b
@everywhere using AsyncDecOPF
@everywhere using DataFrames
@everywhere using Memento

using JLD
using JuMP,Ipopt

@everywhere Memento.setlevel!(Memento.getlogger(InfrastructureModels), "error")
@everywhere Memento.setlevel!(Memento.getlogger(PowerModels), "error")
@everywhere Memento.setlevel!(Memento.getlogger(DecentralizedPowerModels), "error")
@everywhere Memento.setlevel!(Memento.getlogger(AsyncDecOPF), "error")

file = "test/data/case118.m"

N_alpha = 1
N_beta = 1
N_sigma = 1
N_delta = 1
N_tirages = 20
N_comp = 10

A = (N_alpha ≠ 1) ? range(0.0, 30.0, length=N_alpha) : (20.0:20.0)
B = (N_beta ≠ 1) ? range(0.0, 10.0, length=N_beta) : (10.0:10.0)
S = (N_sigma ≠ 1) ? range(0.0, 1.0, length=N_sigma) : (0.5:0.5)
# D = (N_delta ≠ 1) ? range(0.0, 1.0, length=N_delta+1)[2:end] : (1.0:1.0)
D = [0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9, 1.0,
		0.05, 0.15, 0.25, 0.35, 0.45, 0.55, 0.65, 0.75, 0.85, 0.95,]
# D = [0.1, 0.5, 0.9]
# D = [0.2, 0.7, 0.8]
# D = [1.0]
# D = [0.01, 0.02, 0.03, 0.04, 0.05, 0.06, 0.07, 0.08, 0.09,
# 		0.11, 0.12, 0.13, 0.14, 0.15, 0.16, 0.17, 0.18, 0.19,
# 		0.21, 0.22, 0.23, 0.24, 0.25, 0.26, 0.27, 0.28, 0.29,
# 		0.31, 0.32, 0.33, 0.34, 0.35, 0.36, 0.37, 0.38, 0.39,
# 		0.41, 0.42, 0.43, 0.44, 0.45, 0.46, 0.47, 0.48, 0.49,
# 		0.51, 0.52, 0.53, 0.54, 0.55, 0.56, 0.57, 0.58, 0.59,
# 		0.61, 0.62, 0.63, 0.64, 0.65, 0.66, 0.67, 0.68, 0.69,
# 		0.71, 0.72, 0.73, 0.74, 0.75, 0.76, 0.77, 0.78, 0.79,
# 		0.81, 0.82, 0.83, 0.84, 0.85, 0.86, 0.87, 0.88, 0.89,]
T = (1:N_tirages)
# T = (2:N_tirages+1)
COMP = (N_comp ≠ 1) ? range(0.0, 20.0, length=N_comp) : (0.0:0.0)

Eprim = 1e-9 		# La simulation s’arrête lorsque tq ∑ϵp_n ≤ Eprim

ΔT = 8000.0
δt = 10
Tsimu = 30000

ipopt_solver = optimizer_with_attributes(Ipopt.Optimizer,
	"tol"=>1e-8, "print_level"=>2, "constr_viol_tol"=>1e-6)

result_ac_opf = PowerModels.run_opf(file, PowerModels.ACRPowerModel, ipopt_solver)
objective = result_ac_opf["objective"]
result_ac_opf = result_ac_opf["solution"]

filename = "case118_erseghe_sigma0_5_tirages.jld"
JLDfile = JLDFile(filename, Dict(	
										"alpha"	=> collect(A),
										"beta" => collect(B),
										"sigma" => collect(S), 
										"delta" => collect(D),
										"tirages" => collect(T),
										"DeltaT" => [ΔT],
										"sampling_period"=> [δt],
										"Tsimu" => [Tsimu],
										"asyncType" => ["busSync"],
										"comp_time" => collect(COMP),
									);
					)
DF = AsyncDecOPF.computingQueue(JLDfile)
N_points=size(DF,1)