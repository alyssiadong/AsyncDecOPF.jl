function save_results(rc::RemoteChannel, N_points::Int, JLDfile::JLDFile)

	n = N_points
	while n>0
		monitor, df = take!(rc)
		NID = df.NID
		AsyncDecOPF.addpoint(JLDfile, NID, monitor)
		println(n, " saved.")
		n = n-1
	end
end
 
@everywhere function launch_computation( df::DataFrames.DataFrameRow, rc::RemoteChannel, 
	file::String, Eprim::Number, dic_results::Dict=Dict())

	α = df.alpha
	β = df.beta
	σ = df.sigma
	delta = df.delta
	δt = df.sampling_period
	Tsimu = df.Tsimu
	ΔT = df.DeltaT
	asyncType = df.asyncType
	comp_time = df.comp_time

	monitor_max_sample = Tsimu ÷ δt

	ds = instantiate_model_dec_erseghe(file,
				PowerModels.ACPPowerModel, build_dec_opf_erseghe;
				monitoring=true, monitor_max_time = monitor_max_sample,
				optimal_result = dic_results
				)
	# ds = instantiate_model_dec_b2b(file,
	# 			PowerModels.ACPPowerModel, build_dec_opf_erseghe;
	# 			monitoring=true, monitor_max_time = monitor_max_sample,
	# 			optimal_result = dic_results
	# 			)
	asyncProb = AsyncDecOPF.async_problem(ds)

	commPar = AsyncDecOPF.commParams(α,β,σ)

	comp = comp_time .*ones(length(ds.pms))
	compPar = AsyncDecOPF.compParams(comp)

	algoPar = AsyncDecOPF.BusSyncParams(asyncProb, delta, ΔT)
	# algoPar = AsyncDecOPF.AsyncParams(asyncProb, delta, ΔT)

	#if asyncType == "busSync"
	#	display(typeof(AsyncDecOPF.BusSyncParams))
	#	algoPar = AsyncDecOPF.BusSyncParams(asyncProb, delta, ΔT)
	#else
	#	error()
	#end
	try
		result_dec_opf, monitor = AsyncDecOPF.run_simulation(Tsimu, δt, ds, asyncProb,
															commPar, compPar, algoPar, Eprim)
		put!(rc, (monitor, df))
	catch ex
		println("Simulation number $(df.NID) got an error...")
		rethrow(ex)
	end
	println("Point $(df.NID) done")
end

rc = RemoteChannel(()->
	Channel{Tuple{Dict, DataFrames.DataFrameRow}}(N_points))

@async save_results(rc, N_points, JLDfile)

@sync @distributed for i in range(1,stop=size(DF,1))
	launch_computation(DF[i,:], rc, file, Eprim, result_ac_opf) 
end

