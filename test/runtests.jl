import PowerModels
import DecentralizedPowerModels
using AsyncDecOPF
using Test
using LightGraphs, SimpleWeightedGraphs
using ResumableFunctions, SimJulia

import InfrastructureModels

import Memento

import JuMP, Ipopt

# Suppress warnings during testing.
Memento.setlevel!(Memento.getlogger(InfrastructureModels), "error")
Memento.setlevel!(Memento.getlogger(PowerModels), "error")
Memento.setlevel!(Memento.getlogger(DecentralizedPowerModels), "error")
AsyncDecOPF.logger_config!("error")

# Solver
ipopt_solver = JuMP.optimizer_with_attributes(Ipopt.Optimizer, 
		"tol"=>1e-8, "print_level"=>2, "constr_viol_tol"=>1e-6)

@testset "AsyncDecOPF.jl" begin
    include("powernetwork.jl")
    include("communicationdelays.jl")
    include("decopfsimulation.jl")
end