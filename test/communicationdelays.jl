@testset "Instantiate commParams structure" begin
	param = commParams(5.0,1.0,0.01)
	@test param.α == 5.0
	@test param.β == 1.0
	@test param.σ == 0.01
	@test isapprox(AsyncDecOPF.commDelay(0.5, param), 3.38; atol = 0.5)
end

@testset "Compute delays" begin
	file = "../test/data/case118.m"
	ds = DecentralizedPowerModels.instantiate_model_dec_erseghe(file, 
			PowerModels.ACPPowerModel, DecentralizedPowerModels.build_dec_opf_erseghe)
	asyncProb = async_problem(ds)
	commParam = commParams(5.0,1.0,0.01)
	@test isapprox(AsyncDecOPF.sampleDelayValues(1,2,commParam, asyncProb), 2.78; atol=0.3)
	@test isapprox(AsyncDecOPF.meanDelay(commParam, asyncProb), 3.77; atol=0.02)
	@test isapprox(AsyncDecOPF.minimumDelay(commParam, asyncProb), 2.88; atol=0.02)
end
