@testset "Instantiate asynchronous decentralised Erseghe power model" begin
	file = "../test/data/case118.m"
	ds = DecentralizedPowerModels.instantiate_model_dec_erseghe(file, 
			PowerModels.ACPPowerModel, DecentralizedPowerModels.build_dec_opf_erseghe)
	@test length(ds.pms) == 5

	ap = async_problem(ds)
	@test length(ap.duplicates_bus_list[4]["j"])==9
	@test haskey(ap.duplicates_bus_list[1]["j"], 38)
	@test has_edge(ap.comm_graph_region, 1,2)
	@test isapprox(ap.comm_graph_region.weights[1,2], 0.376; atol=1e-2)
end

@testset "Instantiate asynchronous decentralised B2B power model" begin
	file = "../test/data/case57.m"
	ds = DecentralizedPowerModels.instantiate_model_dec_b2b(file, 
			PowerModels.ACPPowerModel, DecentralizedPowerModels.build_dec_opf_b2b)
	@test length(ds.pms) == 8

	ap = async_problem(ds)
	@test length(ap.duplicates_bus_list[4]["j"])==8
	@test haskey(ap.duplicates_bus_list[1]["j"],23)
	@test haskey(ap.duplicates_bus_list[1]["j"],24)
	@test has_edge(ap.comm_graph_region, 4,7)
	@test isapprox(ap.comm_graph_region.weights[4,1],0.335; atol=1e-2)
end

@testset "Test optimal result" begin
	file = "../test/data/case118.m"
	result_ac_opf = AsyncDecOPF.optimal_res(file)

	@test isapprox(result_ac_opf["objective"], 129660; atol = 1)
end
