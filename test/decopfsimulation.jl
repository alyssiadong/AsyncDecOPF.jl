
@testset "AlgoParams Erseghe test" begin 
	file = "../test/data/case57.m"
	ds = DecentralizedPowerModels.instantiate_model_dec_erseghe(file, 
			PowerModels.ACPPowerModel, DecentralizedPowerModels.build_dec_opf_erseghe)
	ap = async_problem(ds)

	algoPar = AsyncParams(ap,1.0, 1000)
	@test length(algoPar.N_trig) == length(ds.pms)
	@test length(algoPar.T_max) == length(ds.pms)
	@test algoPar.N_trig[6] == 10

	algoPar = BusSyncParams(ap,1.0,1000)
	@test length(algoPar.N_trig_bus) 	== length(ds.pms)
	@test length(algoPar.N_trig_reg) 	== length(ds.pms)
	@test length(algoPar.T_max) 		== length(ds.pms)
	@test haskey(algoPar.N_trig_bus[6], 54)
	@test algoPar.N_trig_bus[6][7] == 2
	@test algoPar.N_trig_bus[6][8] == 2
	@test algoPar.N_trig_reg[6] == 7

	algoPar = FullSyncParams(ap,1000)
	@test length(algoPar.N_trig_tot) == length(ds.pms)
	@test algoPar.N_trig_tot[6] == 10
end

@testset "AlgoParams B2B test" begin 
	file = "../test/data/case57.m"
	ds = DecentralizedPowerModels.instantiate_model_dec_b2b(file, 
			PowerModels.ACPPowerModel, DecentralizedPowerModels.build_dec_opf_b2b)
	ap = async_problem(ds)

	algoPar = AsyncParams(ap,1.0, 1000)
	@test length(algoPar.N_trig) == length(ds.pms)
	@test length(algoPar.T_max) == length(ds.pms)
	@test algoPar.N_trig[6] == 8

	algoPar = BusSyncParams(ap,1.0,1000)
	@test length(algoPar.N_trig_bus) 	== length(ds.pms)
	@test length(algoPar.N_trig_reg) 	== length(ds.pms)
	@test length(algoPar.T_max) 		== length(ds.pms)
	@test haskey(algoPar.N_trig_bus[6], 54)
	@test algoPar.N_trig_bus[6][7] == 2
	@test algoPar.N_trig_bus[6][8] == 1
	@test algoPar.N_trig_reg[6] == 7

	algoPar = FullSyncParams(ap,1000)
	@test length(algoPar.N_trig_tot) == length(ds.pms)
	@test algoPar.N_trig_tot[6] == 8
end

@testset "Message structure" begin
	mes = Message(1,2,31,5,1+im*0.9)

	@test AsyncDecOPF.bus(mes)==31
	@test AsyncDecOPF.receiverRegion(mes)==2
	@test AsyncDecOPF.senderRegion(mes)==1
	@test AsyncDecOPF.iter(mes)==5
	@test AsyncDecOPF.idx(mes)==(0,0,0)

	mes = Message(1,2,31,5,(31,32,1),1+im*0.9)
	@test AsyncDecOPF.idx(mes)==(31,32,1)
end

@testset "Mailbox Manager" begin

	# To do: Find a way to test the @resumable functions...

	# @resumable function testMailboxManager(env::Simulation, mailbox)
	# 	@yield @process AsyncDecOPF.leaveMessage(env,1,6,26,4,1+0.1im,mailbox)

	# 	@test AsyncDecOPF.countMessages(mailbox)==1
	# 	@test AsyncDecOPF.countMatchingMessages(mailbox)==0

	# 	@yield @process AsyncDecOPF.leaveMessage(env,1,6,27,1,2+0.1im,mailbox)

	# 	@test AsyncDecOPF.countMessages(mailbox)==2
	# 	@test AsyncDecOPF.countMatchingMessages(mailbox)==1

	# 	mes, mes_back = @process AsyncDecOPF.readMessage(env, 26, 6, mailbox)
	# 	@test length(mes_back) == 1
	# 	@test AsyncDecOPF.iter(mes) == 0
	# 	@test AsyncDecOPF.value(mes) == 2+0.1im
	# end

    file = "../test/data/case57.m"
	ds = DecentralizedPowerModels.instantiate_model_dec_erseghe(file, 
			PowerModels.ACPPowerModel, DecentralizedPowerModels.build_dec_opf_erseghe)
	ap = async_problem(ds)
	env = Simulation()

	mailbox = MailboxManager(env, 6, ap)
	@test keys(mailbox.counter) == keys(ap.duplicates_bus_list[6]["j"])
	@test mailbox.init_done_flag == false
	@test length(mailbox.counter[7])==2
	@test AsyncDecOPF.countMatchingMessages(mailbox)==0
	@test AsyncDecOPF.countMessages(mailbox)==0

	# @process testMailboxManager(env, mailbox)
	# run(env)
end

# @testset ""