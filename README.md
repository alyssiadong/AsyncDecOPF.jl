# AsyncDecOPF

[![Dev](https://img.shields.io/badge/docs-dev-blue.svg)](https://alyssiadong.gitlab.io/AsyncDecOPF.jl/dev)
[![Build Status](https://gitlab.com/alyssiadong/AsyncDecOPF.jl/badges/master/pipeline.svg)](https://gitlab.com/alyssiadong/AsyncDecOPF.jl/pipelines)
[![Coverage](https://gitlab.com/alyssiadong/AsyncDecOPF.jl/badges/master/coverage.svg)](https://gitlab.com/alyssiadong/AsyncDecOPF.jl/commits/master)

Asynchronous decentralized OPF, which simulates message exchanges between computational agents. 